/* **********************************************************************
    Module: BITEK.C - BITEK.
    Purpose: Implementation of BITEK module.
    Version: 0.01                                   04:28PM  2013/03/18
    Compiler: Keil 8051 C Compiler v9.51
    Reference:
   ********************************************************************** */
#define _BITEK_C_

#include "BIT1628B.h"
#include "i2c.h"
#include "OSD.H"
#include <stdio.h>
#include <string.h>


extern  I2C_HandleTypeDef BITEK_I2C;

const uint8_t abPLL1_198_19E[]     ={0x09,0x5A,0x03,0x09,0x5A,0x03,0x0A}; 
const uint8_t abPLL1_198_19E_PAL[] ={0x08,0x49,0x03,0x09,0x5A,0x03,0x0A}; 
const uint8_t abVP_130_145[]       ={0x80,0x80,0x80,0x80,0x80,0x80,0xC6,0x39,0x30,0x80,0x80,0x80,0x40,0x02,0x40,0x40,0x04,0xAA,0x38,0xC2,0xCC,0x2B};

uint8_t REG_Add = 0;
uint8_t BITEK_TX_Buf[TX_BUF_Size]={0};
uint8_t BITEK_RX_Buf[RX_BUF_Size]={0};
uint8_t BITEK_I2C_Add=0;
uint8_t STD_Format=0x50;

const uint8_t apbCOLOR_STD[9] =   
														{    
																0,/* PAL */
																1,/* PAL-N */
																2,/* SECAM */  
																3,/* PAL-M */ 
																4,/* NTSC443 50 */
																5,/* NTSC-M */
																6,/* NTSC443 60 */
																7,/* BlackWhite */
																8,/*No Signal */
														};   
//==========================================================================
const uint8_t abVP2_00A_0FE[] = {
      // 0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F   
/* 0 */                                                   0x12,0x81,0xFC,0xA9,0xA9,0x80,
/* 1 */ 0x82,0x00,0x00,0x00,0x22,0x00,0x00,0x00,0x10,0x30,0x30,0x40,0x44,0x44,0x44,0x06,
/* 2 */ 0x26,0x33,0x39,0x4F,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x5C,0x00,0x00,0x00,0x00,
/* 3 */ 0x00,0x00,0x00,0x00,0x00,0x80,0x00,0x01,0x17,0x80,0x17,0x03,0x40,0x00,0x07,0x3F,
/* 4 */ 0x3F,0x3F,0x3F,0x84,0xE0,0x38,0x7D,0x4D,0x30,0x15,0x05,0x10,0x7D,0x4D,0x30,0x15,
/* 5 */ 0x05,0x10,0x40,0x58,0x88,0xC3,0xD3,0xFF,0xFF,0x33,0x44,0x44,0x40,0x30,0x30,0x40,
/* 6 */ 0x26,0x33,0x03,0x30,0x00,0x04,0xD9,0xB2,0x00,0x00,0x02,0x88,0x06,0x00,0x01,0x00,
/* 7 */ 0xD9,0xB2,0xD4,0x8C,0xD9,0xB2,0xD4,0x8C,0x00,0x00,0x00,0x00,0x00,0x00,0x14,0x28,
/* 8 */ 0x50,0x00,0x40,0x01,0x13,0x27,0x4E,0x36,0x00,0x01,0x13,0x00,0x00,0x00,0x11,0xFB,
/* 9 */ 0x00,0x00,0xFB,0x00,0x00,0x00,0xA4,0x03,0x00,0x7B,0x7B,0xFF,0xAC,0xF2,0x04,0xB3,
/* A */ 0x04,0x04,0x00,0x40,0x32,0x78,0x80,0x00,0x00,0x00,0x0D,0x0D,0x00,0x88,0xA0,0x40,
/* B */ 0x19,0x85,0x85,0xEA,0xA4,0x32,0x7F,0x31,0x80,0xFE,0x40,0x32,0x00,0x08,0x00,0x00,
/* C */ 0x4D,0xFB,0x71,0x80,0xC3,0x92,0x9B,0x01,0x9B,0x01,0x92,0x81,0x00,0xD0,0x00,0x00,
/* D */ 0x00,0x10,0x80,0x5E,0xAD,0x04,0x04,0x00,0x00,0x00,0x15,0x00,0x3F,0x8E,0xE8,0x8B,
/* E */ 0x03,0x00,0x00,0x00,0x02,0x00,0x00,0x00,0x01,0x00,0x00,0x03,0x00,0x00,0x00,0x00,
/* F */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0F,0x0F,0x0F,0x7F 
        };	

const uint8_t abVP2_00A_0FE_PAL[] = {
      // 0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F   
/* 0 */                                                   0x12,0x81,0xFC,0xA9,0xA9,0x80,
/* 1 */ 0x82,0x00,0x00,0x00,0x22,0x00,0x00,0x00,0x10,0x30,0x30,0x40,0xB0,0xB0,0x44,0x06,
/* 2 */ 0x26,0x33,0x84,0x4F,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x5C,0x00,0x00,0x00,0x00,
/* 3 */ 0x00,0x00,0x00,0x00,0x00,0x80,0x00,0x01,0x17,0x80,0x17,0x03,0x40,0x00,0x07,0x3F,
/* 4 */ 0x3F,0x3F,0x3F,0x84,0xE0,0x38,0x9B,0x6B,0x30,0x15,0x35,0x10,0x9B,0x6B,0x30,0x15,
/* 5 */ 0x35,0x10,0x40,0x58,0x88,0xC3,0xD3,0xFF,0xFF,0x33,0x44,0x44,0x40,0x30,0x30,0x40,
/* 6 */ 0x26,0x33,0x03,0x30,0x00,0x04,0xD9,0xB2,0x00,0x00,0x02,0x88,0x06,0x00,0x01,0x00,
/* 7 */ 0xD9,0xB2,0xD4,0x8C,0xD9,0xB2,0xD4,0x8C,0x00,0x00,0x00,0x00,0x00,0x00,0x17,0x2F,
/* 8 */ 0x5E,0x3A,0x20,0x01,0x17,0x2F,0x5E,0x3A,0x20,0x01,0x13,0x00,0x00,0x00,0x11,0xFB,
/* 9 */ 0x00,0x00,0xFB,0x00,0x00,0x00,0xA4,0x03,0x00,0x7B,0x7B,0xFF,0xAC,0xF2,0x04,0xB3,
/* A */ 0x04,0x04,0x00,0x40,0x32,0x78,0x80,0x00,0x00,0x00,0x0D,0x0D,0x00,0x88,0xA0,0x40,
/* B */ 0x19,0x85,0x85,0xEA,0xA4,0x32,0x7F,0x31,0x80,0xFE,0x40,0x32,0x00,0x08,0x00,0x00,
/* C */ 0x4D,0xFB,0x71,0x80,0xC3,0x92,0x9B,0x01,0x9B,0x01,0x92,0x81,0x00,0xD0,0x00,0x00,
/* D */ 0x00,0x10,0x80,0x5E,0xAD,0x04,0x04,0x00,0x00,0x00,0x15,0x00,0x3F,0x8E,0xE8,0x8B,
/* E */ 0x03,0x00,0x00,0x00,0x02,0x00,0x00,0x00,0x01,0x00,0x00,0x03,0x00,0x00,0x00,0x00,
/* F */ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0F,0x0F,0x0F,0x7F 
        };					
//==========================================================================
bool WriteRegister_8b(uint16_t reg_add,uint8_t value) 
{
	REG_Add = reg_add & 0x00FF;
  BITEK_I2C_Add=(((reg_add >> 8) & 0x00FF) <<1) | (BITEK_I2C_MSB);
	memset(BITEK_TX_Buf,0,sizeof(BITEK_TX_Buf));
	BITEK_TX_Buf[0] = REG_Add;	
	BITEK_TX_Buf[1] = value;	
  
	if(HAL_I2C_Master_Transmit(&BITEK_I2C,(BITEK_I2C_Add << 0),BITEK_TX_Buf,2,100) == HAL_OK)
		 {
			my_printf("Write Register 8b OK \r\n"); 
			return 1;
		 }
	else
	  {
			my_printf("Write Register 8b Error\r\n"); 
			return 0;	
		}		
}
//==========================================================================
uint8_t ReadRegister_8b(uint16_t reg_add)
{
	REG_Add = reg_add & 0x00FF;
  BITEK_I2C_Add=(((reg_add >> 8) & 0x00FF) <<1) | (BITEK_I2C_MSB);
	memset(BITEK_TX_Buf,0,sizeof(BITEK_TX_Buf));
	memset(BITEK_RX_Buf,0,sizeof(BITEK_RX_Buf));
	BITEK_TX_Buf[0] = REG_Add;	
	if(HAL_I2C_Master_Transmit(&BITEK_I2C,(BITEK_I2C_Add << 0), BITEK_TX_Buf, 1, 100) == HAL_OK)
	  {
	    if(HAL_I2C_Master_Receive(&BITEK_I2C,(BITEK_I2C_Add << 0), BITEK_RX_Buf, 1, 100) == HAL_OK)
			 {
				my_printf("Read Register 8b =%2X \r\n",BITEK_RX_Buf[0]); 
				return BITEK_RX_Buf[0];
			 }
			else
			 {
				my_printf("Read Register 8b Error\r\n"); 
				return 0;	
			 }	
		}
	else
	  {
			my_printf("Read Register 8b Error\r\n"); 
			return 0;	
		}			
}
//==========================================================================
bool WriteRegister_Burst(uint16_t reg_add,uint8_t *array,uint16_t len)
{ 
	REG_Add = reg_add & 0x00FF;
	BITEK_I2C_Add=(((reg_add >> 8) & 0x00FF) <<1) | (BITEK_I2C_MSB);
	
	memset(BITEK_TX_Buf,0,sizeof(BITEK_TX_Buf));
	BITEK_TX_Buf[0] = REG_Add;	
	
  for(uint16_t i=0; i<len; i++) 
	    BITEK_TX_Buf[i+1]=array[i];
	
	if(HAL_I2C_Master_Transmit(&BITEK_I2C,(BITEK_I2C_Add << 0),BITEK_TX_Buf,len+1,100)==HAL_OK)
	 {
		my_printf("Write Register Burst OK\r\n"); 
		return 1;
	 }
	else
	 {
		my_printf("Write Register Burst Error\r\n"); 
		return 0;	
	 }		 
}
//==========================================================================
bool ReadRegister_Burst(uint16_t reg_add, uint8_t *read_data,uint8_t len) 
{
	REG_Add = reg_add & 0x00FF;
	BITEK_I2C_Add=(((reg_add >> 8) & 0x00FF) <<1) | (BITEK_I2C_MSB);
	memset(BITEK_TX_Buf,0,sizeof(BITEK_TX_Buf));
	memset(BITEK_RX_Buf,0,sizeof(BITEK_RX_Buf));
	BITEK_TX_Buf[0] = REG_Add;	
	if(HAL_I2C_Master_Transmit(&BITEK_I2C,(BITEK_I2C_Add << 0), BITEK_TX_Buf, 1, 100) == HAL_OK)
	  {
	    if(HAL_I2C_Master_Receive(&BITEK_I2C,(BITEK_I2C_Add << 0), BITEK_RX_Buf, len, 100) == HAL_OK)
			 {
				my_printf("Read Register Burst =%2X \r\n",BITEK_RX_Buf[0]); 
				return 1;
			 }
			else
			 {
				my_printf("Read Register Burst Error\r\n"); 
				return 0;	
			 }	
		}
	else
	  {
			my_printf("Read Register Burst Error\r\n"); 
			return 0;	
		}		
}
//==========================================================================
void WriteRegister_Repeat(uint16_t reg_add,uint8_t value,uint8_t count)
{
    for(uint8_t i=0; i<count; i++) 
	     {
			   WriteRegister_8b(reg_add+i,value);			 
			 }
}
//==========================================================================	
void BITEK_version(void)
{
     ReadRegister_8b(BITEK_000_HW_VER_INFO);
	   my_printf("BITEK Chip info :\r\n"); 
		 my_printf("Product Version : %d \r\n", (BITEK_RX_Buf[0] & 0x03)); 
		 my_printf("Product Number  : %d \r\n", (BITEK_RX_Buf[0] & 0x1C)>>2); 
		 my_printf("Product Group   : %d \r\n", (BITEK_RX_Buf[0] & 0xE0)>>5); 
	
     ReadRegister_8b(BITEK_001_SW_VER);
		 my_printf("Software version: %d \r\n", (BITEK_RX_Buf[0] & 0xFF)); 	 
}
//==========================================================================
void BITEK_SetBrightness (uint8_t Brightness)
{
    //WriteRegister_8b(BITEK_13A_BRIGHTNESS, BRIGHTNESS_OFFSET + BRIGHTNESS_SLOPE * Brightness);
	  WriteRegister_8b(BITEK_13A_BRIGHTNESS, Brightness);
} 
//==========================================================================
void BITEK_SetContrast (uint8_t Contrast)
{
    WriteRegister_8b(BITEK_13B_CONTRAST, CONTRAST_OFFSET + CONTRAST_SLOPE * Contrast);
} 
//==========================================================================
void BITEK_TestPattern(_Bool STATE)
{
    uint8_t     b42;
    ReadRegister_8b(BITEK_042_TESTPAT_ATTR);
	  b42=BITEK_RX_Buf[0];// & (VP_MASK_PATTERN_HV | VP_MASK_PATTERN_DIR | VP_MASK_PATTERN_TYPE);;	 

    if(STATE)	
		{
      my_printf("Test Battern Enabled \r\n");
      WriteRegister_8b(BITEK_03F_R_ATTR, 0x3F);
      WriteRegister_8b(BITEK_040_G_ATTR, 0x3F);
      WriteRegister_8b(BITEK_041_B_ATTR, 0x3F);
      WriteRegister_8b(BITEK_042_TESTPAT_ATTR, b42 | 0x80); //| bMode | bPatternType
		}
		else
		{
			WriteRegister_8b(BITEK_042_TESTPAT_ATTR, b42 & 0x3F);
		  my_printf("Test Battern Disabled\r\n");
		}
} 
//==========================================================================
void VP_STD_Detect (void)
{
    #define MSG_COLOR_STD_SIZE      10   
    uint8_t bSTD=0; 
    // H-LOCK Ready , Auto Sync Detection Ready ,Auto Color Standard Detection Ready	
     ReadRegister_8b(BITEK_1E5_VD_INFO_O);
	   bSTD = BITEK_RX_Buf[0] & 0x07;
	   my_printf("BITEK_1E5_VD_INFO_O = %2X \r\n",BITEK_RX_Buf[0]); 
    //if((bSTD & 0x02) == 0x02) 
    if(bSTD == 0x0E)   
       { 
         //SYNC READY (0x1BC[7]) = 1 
         ReadRegister_8b(BITEK_1E6_STD_INFO_O); 				 
         bSTD = BITEK_RX_Buf[0] & 0x07; 
         //OSD_ShowHex(VP_SHOW_VD_STATUS + 6, bSTD);          // ROW5: Debug Only				 
       }   
    else   
        bSTD = 8; //No Signal Detected    
} 

//==========================================================================
void BITEK_Status(void)
{
	 my_printf("#################################################\r\n"); 
	 ReadRegister_8b(BITEK_1E5_VD_INFO_O);
	 my_printf("Video Decoder Status Register 0x1E5 = 0x%.2X \r\n", (BITEK_RX_Buf[0])); 
	 if(BITEK_RX_Buf[0] & BITEK_MASK_VD_READY_O)
		 my_printf("Video Decoder 0x1E5[0] : Ready\r\n"); 
	 else
     my_printf("Video Decoder 0x1E5[0] : Not Ready\r\n"); 

	 if(BITEK_RX_Buf[0] & BITEK_MASK_VD_HLCK_O)
		 my_printf("H-LOCK 0x1E5[1] : Ready\r\n"); 
	 else
     my_printf("H-LOCK 0x1E5[1] : Not Ready\r\n"); 

	 if(BITEK_RX_Buf[0] & BITEK_MASK_SYNC_READY_O)
	   {
		   my_printf("Auto Sync Detection 0x1E5[2] : Ready\r\n"); 
	   }
	 else
	   {
       my_printf("Auto Sync Detection 0x1E5[2] : Not Ready\r\n"); 
		 }

	 if(BITEK_RX_Buf[0] & BITEK_MASK_STD_READY_O)
		 my_printf("Auto Color Standard Detection 0x1E5[3] : Ready\r\n"); 
	 else
     my_printf("Auto Color Standard Detection 0x1E5[3] : Not Ready\r\n"); 
	 
	 ReadRegister_8b(BITEK_1E6_STD_INFO_O);
	 my_printf("Video Decoder Status Register 0x1E6 = 0x%.2X \r\n", (BITEK_RX_Buf[0])); 
   switch (BITEK_RX_Buf[0] & 0x0F)
            {
                case BITEK_MASK_STD_PAL_M:              // 3 + 8
									my_printf("Color Standard Detection Result : PAL_M(60Hz)\r\n"); 
								  break;
                case BITEK_MASK_STD_NTSC_M:             // 5 + 8
									my_printf("Color Standard Detection Result : NTSC_M(60Hz)\r\n"); 
								  break;
                case BITEK_MASK_STD_NTSC443_60:         // 6 + 8
									my_printf("Color Standard Detection Result : NTSC443(60Hz)\r\n"); 
								  break;
                case BITEK_MASK_STD_BLACK_WHITE60:      // 7 + 8
									my_printf("Color Standard Detection Result : BLACK_WHITE(60Hz)\r\n");
                  break;								
                case BITEK_MASK_STD_PAL:                // 0
									my_printf("Color Standard Detection Result : PAL(50Hz)\r\n"); 
								  break;
                case BITEK_MASK_STD_PAL_N:              // 1
									my_printf("Color Standard Detection Result : PAL_N(50Hz)\r\n"); 
								  break;
                case BITEK_MASK_STD_SECAM:              // 2
									my_printf("Color Standard Detection Result : SECAM(50Hz)\r\n"); 
								  break;
                case BITEK_MASK_STD_NTSC443_50:         // 4
									my_printf("Color Standard Detection Result : NTSC443(50Hz)\r\n"); 
								  break;
                case BITEK_MASK_STD_BLACK_WHITE50:      // 7
									my_printf("Color Standard Detection Result : BLACK_WHITE(50Hz)\r\n");
								  break;
                case BITEK_MASK_STD_PAL_60:             // 8
									my_printf("Color Standard Detection Result : PAL(60Hz)\r\n");
								  break;
                default:
                    break;
            } 
   switch (BITEK_RX_Buf[0] & 0x30)
            {
                case 0X00:       
									my_printf("Color Burst Detection : 3.57MHz\r\n"); 
								  break;
                case 0X10:       
									my_printf("Color Burst Detection : 4.2MHz\r\n"); 
								  break;
                case 0X20:       
									my_printf("Color Burst Detection : 4.3MHz\r\n"); 
								  break;
                case 0X30:       
									my_printf("Non Standard\r\n"); 
								  break;									
                default:
                    break;
            } 
						
   if(BITEK_RX_Buf[0] & BITEK_MASK_COLORDET_O)
       		 my_printf("Color detection result :  Color source\r\n");
	 else
		       my_printf("Color detection result : No color or low color \r\n");

	 ReadRegister_8b(BITEK_1CD_DET_STATUS_O);
	 my_printf("Auto Detection Register 0x1CD = 0x%.2X \r\n", (BITEK_RX_Buf[0])); 
	 if(BITEK_RX_Buf[0] & BITEK_MASK_VD_STDCHG_O)
		 my_printf("Auto on Status 0x1CD[4] : Free run Mode\r\n"); 
	 else
     my_printf("Auto on Status 0x1CD[4] : Normal\r\n");  
	 
	   ReadRegister_8b(0x1c5);
	   my_printf("0x1C5 = %2X \r\n",BITEK_RX_Buf[0]); 
	   ReadRegister_8b(0x1c6);
	   my_printf("0x1C56 = %2X \r\n",BITEK_RX_Buf[0]); 	
}
//======================================================================
bool uDisplay_READ_Burst(uint16_t reg_add,uint8_t len) 
{
	memset(BITEK_RX_Buf,0,sizeof(BITEK_RX_Buf));
	BITEK_TX_Buf[0] = reg_add;	
	if(HAL_I2C_Master_Transmit(&BITEK_I2C,(0xBB << 0), BITEK_TX_Buf, 1, 100) == HAL_OK)
	  {
	    if(HAL_I2C_Master_Receive(&BITEK_I2C,(0xBB << 0), BITEK_RX_Buf, len, 100) == HAL_OK)
			 {
				my_printf("uDisplay_READ_Burst  OK \r\n"); 
				for(char i=0; i<len; i++)
					 {	 
							my_printf("0x%.2X,",BITEK_RX_Buf[i]); 
					 }	
				my_printf("\r\n");				
				return 1;
			 }
			else
			 {
				my_printf("uDisplay_READ_Burst  Error\r\n"); 
				return 0;	
			 }	
		}
	else
	  {
			my_printf("uDisplay_READ_Burst  Error\r\n"); 
			return 0;	
		}		
}
//======================================================================
bool uDisplay_Write_8b(uint16_t reg_add,uint8_t value) 
{
	BITEK_TX_Buf[0] = reg_add;
  BITEK_TX_Buf[1] = value;		
	if(HAL_I2C_Master_Transmit(&BITEK_I2C,(0xBB << 0), BITEK_TX_Buf, 2, 100) == HAL_OK)
	  {		
        my_printf("uDisplay_Write_8b OK\r\n"); 			
				return 1;
		}
	else
	  { 
			  return 0;	
		}		
}
//======================================================================
void BITEK_Init1024(void)
{
         
    if(BITEK_SYNC_DETECT() && BITEK_DETECT_HZ())
		  {
				STD_Format=0x60;
				BITEK_Init_60HZ();
      }
		else
	  	{	
        STD_Format=0x50;	
        BITEK_Init_50HZ();				
			}
    
	  WriteRegister_Burst(BITEK_130_BRIGHTNESS_R,(uint8_t *)abVP_130_145, sizeof(abVP_130_145));		// VP Brightness Default Values !
	
}	
//======================================================================
void BITEK_OCLK_POL(_Bool POL)
{
	 uint8_t bREG=0;
	 ReadRegister_8b(0x00B);
   if(POL == 0)	
	    bREG = BITEK_RX_Buf[0] & 0xFD; 
	 else
		  bREG = BITEK_RX_Buf[0] | 0x02; 
	 
	 WriteRegister_8b(0x00B,bREG);
	 my_printf("OCLK POL 0x00B =0x%.2X \r\n",bREG);
}
//===============================================================================================
void BITEK_HSYNC_POL(_Bool POL)
{
	 uint8_t bREG=0;
	 ReadRegister_8b(0x0FB);
   if(POL == 0)	
	    bREG = BITEK_RX_Buf[0] & 0xEF; 
	 else
		  bREG = BITEK_RX_Buf[0] | 0x10; 
	 
	 WriteRegister_8b(0x0FB,bREG);
	 my_printf("HSYNC POL 0x0FB =0x%.2X \r\n",bREG);	
}
//===============================================================================================
void BITEK_VSYNC_POL(_Bool POL)
{
	 uint8_t bREG=0;
	 ReadRegister_8b(0x0FC);
   if(POL == 0)	
	    bREG = BITEK_RX_Buf[0] & 0xEF; 
	 else
		  bREG = BITEK_RX_Buf[0] | 0x10; 
	 
	 WriteRegister_8b(0x0FC,bREG);
	 my_printf("VSYNC POL 0x0FC =0x%.2X \r\n",bREG);		
}
//===============================================================================================
void BITEK_RGB_STATE(_Bool state)
{
   if(state == 1)	
	   {
			 my_printf("BITEK RGB STATE = Enable \r\n");		
	     WriteRegister_8b(0x0FE,0x00); 
		 }
	 else
	   {
			 my_printf("BITEK RGB STATE = Tri-state \r\n");		
		   WriteRegister_8b(0x0FE,0x3F);
	   }	
}
//================================================================================================
_Bool BITEK_SYNC_DETECT(void)
{
	 ReadRegister_8b(BITEK_1E5_VD_INFO_O);
	 if(BITEK_RX_Buf[0] & BITEK_MASK_SYNC_READY_O)
	   {
			 return 1;    // SYNC Signal Detected
	   }
	 else
	   {
			 return 0;    // SYNC Signal NOT Detected
		 }
}
//================================================================================================
_Bool BITEK_DETECT_HZ(void)
{
	 ReadRegister_8b(BITEK_1E6_STD_INFO_O);
	 if(BITEK_RX_Buf[0] & BITEK_MASK_FIDT_O)
	   {
			 return 1;    // 60Hz
	   }
	 else
	   {
			 return 0;    // 50Hz
		 }
}
//================================================================================================
void BITEK_Init_60HZ(void)
{
	 WriteRegister_Burst(BITEK_198_PLL_DM_M0,(uint8_t *)abPLL1_198_19E ,sizeof(abPLL1_198_19E));  
	 WriteRegister_Burst(BITEK_00A_CLK_ATTR0,(uint8_t *)abVP2_00A_0FE, sizeof(abVP2_00A_0FE));
}
//================================================================================================
void BITEK_Init_50HZ(void)
{
	 WriteRegister_Burst(BITEK_198_PLL_DM_M0,(uint8_t *)abPLL1_198_19E_PAL ,sizeof(abPLL1_198_19E_PAL));  
	 WriteRegister_Burst(BITEK_00A_CLK_ATTR0,(uint8_t *)abVP2_00A_0FE_PAL, sizeof(abVP2_00A_0FE_PAL));
}
//================================================================================================
void BITEK_PWM0_Init(void)
{
  WriteRegister_8b(0x17F,0xFF); 
	WriteRegister_8b(0x180,0xA0); 
	WriteRegister_8b(0x181,0x7F);   // Duty = 50%
	WriteRegister_8b(0x182,0x00); 
	WriteRegister_8b(0x183,0x20); 
}
//================================================================================================
void BITEK_PWM0_UPDATE(uint8_t duty)
{
	static uint8_t duty_cycle=0;
	duty_cycle=(uint8_t)(0.01*duty*255);
	WriteRegister_8b(0x181,duty_cycle); 
}
