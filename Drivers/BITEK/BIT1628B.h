/* ::::::::::::::::::::::::::::::::::::
    BITEK Configuration
   :::::::::::::::::::::::::::::::::::: */
#include "stm32l0xx_hal.h"
#include "stdbool.h"


#define TX_BUF_Size                             300
#define RX_BUF_Size                             100

// [1] Version Control   
#define BITEK_I2C                               hi2c2
#define SOURCE_CVBS11		                        ON	        // AIN11
#define SOURCE_CVBS12                           OFF         // AIN12 
#define SOURCE_CVBS2                            OFF         // AIN2 

#define BITEK_I2C_MSB                           0xC0        // = 0xC0 (OP3 Pin (H) ) OR =0x80 (OP3 Pin (L) )
#define BITEK_000_HW_VER_INFO                   0x000       // [1] Hardware version
    #define BITEK_MASK_HW_VER                         0xB3    // 7 6 5 4 3 2 1 0
    #define BITEK_MASK_PRODUCT_VER_O                  0x03    // | | | | | | |_|_ Product Version
    #define BITEK_MASK_PRODUCT_NUM_O                  0x1C    // | | | |_|_|_____ Product Number
    #define BITEK_MASK_PRODUCT_GROUP_O                0xE0    // |_|_|___________ Product Group
                                                              // 1 0 1 1 0 0 1 0  0xB2

#define BITEK_001_SW_VER                        0x001       // [1] Software version

// [1] Interrupt Function
#define BITEK_002_INT0_FLAG_O                   0x002       // [1] Interrupt Flag for INT0
    #define BITEK_MASK_FLAG0_VD_SIGNALREADY         0x01    // Video Decoder Signal ready
    #define BITEK_MASK_FLAG0_VP_SIGNALREADY         0x02    // Video Processor Signal ready
    #define BITEK_MASK_FLAG0_VIDEO_ADC              0x04    // Video ADC changes
    #define BITEK_MASK_FLAG0_VD_MV                  0x08    // Video Decoder Macrovision detection
    #define BITEK_MASK_FLAG0_VD_CC_I2CM             0x10    // Video Decoder CC detection/I2C Master
    #define BITEK_MASK_FLAG0_ADC                    0x20    // ADC interface function
    #define BITEK_MASK_FLAG0_IR_I2CS                0x40    // IR/I2C Slave  function
    #define BITEK_MASK_FLAG0_GPI                    0x80    // GPI function

#define BITEK_003_INT0_MASK                     0x003       // [1] Interrupt Mask for INT0
    #define BITEK_MASK_MASK0_VD_SIGNALREADY         0x01    // (0=Enable Interrupt, 1=Disable Interrupt)
    #define BITEK_MASK_MASK0_VP_SIGNALREADY         0x02
    #define BITEK_MASK_MASK0_VIDEO_ADC              0x04
    #define BITEK_MASK_MASK0_VD_MV                  0x08
    #define BITEK_MASK_MASK0_VD_CC_I2CM             0x10
    #define BITEK_MASK_MASK0_ADC                    0x20
    #define BITEK_MASK_MASK0_IR_I2CS                0x40
    #define BITEK_MASK_MASK0_GPI                    0x80

#define BITEK_004_INT0_ACK                      0x004       // [1] Interrupt ACK for INT0
    #define BITEK_MASK_ACK0_VD_SIGNALREADY          0x01    // (0=Clear and Disable Interrupt, 1=Enable Interrupt)
    #define BITEK_MASK_ACK0_VP_SIGNALREADY          0x02
    #define BITEK_MASK_ACK0_VIDEO_ADC               0x04
    #define BITEK_MASK_ACK0_VD_MV                   0x08
    #define BITEK_MASK_ACK0_VD_CC_I2CM              0x10
    #define BITEK_MASK_ACK0_ADC                     0x20
    #define BITEK_MASK_ACK0_IR_I2CS                 0x40
    #define BITEK_MASK_ACK0_GPI                     0x80

#define BITEK_005_INT1_FLAG_O                   0x005       // [1] Interrupt Flag for INT1
    #define BITEK_MASK_FLAG1_VD_NOSIGNAL            0x01    // Video Decoder No Signal
    #define BITEK_MASK_FLAG1_VP_NOSIGNAL            0x02    // Video Processor No Signal
    #define BITEK_MASK_FLAG1_VP_MODE                0x04    // Video Processor Mode change detection
    #define BITEK_MASK_FLAG1_VD_STANDARD            0x08    // Video Decoder Standard change detection
    #define BITEK_MASK_FLAG1_LINE_BUFFER_I2CM       0x10    // Line Buffer error/I2C Master
    #define BITEK_MASK_FLAG1_ADC                    0x20    // ADC interface function
    #define BITEK_MASK_FLAG1_IR_I2CS                0x40    // IR/I2CS function
    #define BITEK_MASK_FLAG1_GPI                    0x80    // GPI function

#define BITEK_006_INT1_MASK                     0x006       // [1] Interrupt Mask for INT1
    #define BITEK_MASK_MASK1_VD_NOSIGNAL            0x01    // (0=Enable Interrupt, 1=Disable Interrupt)
    #define BITEK_MASK_MASK1_VP_NOSIGNAL            0x02
    #define BITEK_MASK_MASK1_VP_MODE                0x04
    #define BITEK_MASK_MASK1_VD_STANDARD            0x08
    #define BITEK_MASK_MASK1_LINE_BUFFER_I2CM       0x10
    #define BITEK_MASK_MASK1_ADC                    0x20
    #define BITEK_MASK_MASK1_IR_I2CS                0x40
    #define BITEK_MASK_MASK1_GPI                    0x80

#define BITEK_007_INT1_ACK                      0x007       // [1] Interrupt ACK for INT1
    #define BITEK_MASK_ACK1_VD_NOSIGNAL             0x01    // (0=Clear and Disable Interrupt, 1=Enable Interrupt)
    #define BITEK_MASK_ACK1_VP_NOSIGNAL             0x02
    #define BITEK_MASK_ACK1_VP_MODE                 0x04
    #define BITEK_MASK_ACK1_VD_STANDARD             0x08
    #define BITEK_MASK_ACK1_LINE_BUFFER_I2CM        0x10
    #define BITEK_MASK_ACK1_ADC                     0x20
    #define BITEK_MASK_ACK1_IR_I2CS                 0x40
    #define BITEK_MASK_ACK1_GPI                     0x80

#define BITEK_008_INT_ATTR                      0x008       // [1] Interrupt Attribute
    #define BITEK_MASK_INT0_POL                     0x01    // Interrupt POLARITY for INT0 (0=High level or Rising edge active, 1=Low level or Falling edge active)
    #define BITEK_MASK_INT0_TYPE                    0x02    // Interrupt TYPE for INT0 (0=Edge type , 1=Level type )
    #define BITEK_MASK_INT1_POL                     0x04    // Interrupt POLARITY for INT1 (0=High level or Rising edge active, 1=Low level or Falling edge active)
    #define BITEK_MASK_INT1_TYPE                    0x08    // Interrupt TYPE for INT1 (0=Edge type , 1=Level type )
    #define BITEK_MASK_INT_CHT_SEL0                 0x10    // Video ADC change detection (0=Disable, 1=Enable) AIN11
    #define BITEK_MASK_INT_CHT_SEL1                 0x20    // Video ADC change detection (0=Disable, 1=Enable) AIN12
    #define BITEK_MASK_INT_CHT_SEL2                 0x40    // Video ADC change detection (0=Disable, 1=Enable) AIN2
    #define BITEK_MASK_INT_CHT_SEL                  0x70    // Video ADC change detection (0=Disable, 1=Enable)
    #define BITEK_MASK_ADCINT_TYPE                  0x80    // ADC interface interrupt type (0=Edge trigger, 1=Level trigger)

#define BITEK_009_INT_SEL                       0x009       // [1] Interrupt Selection
    #define BITEK_MASK_INT0_ISR6_SEL                0x07
    #define BITEK_MASK_INT0_ISR6_IR                 0x00    // IR
    #define BITEK_MASK_INT0_ISR6_SWR                0x01    // S+W+R
    #define BITEK_MASK_INT0_ISR6_SR                 0x02    // S+R
    #define BITEK_MASK_INT0_ISR6_SW                 0x03    // S+W
    #define BITEK_MASK_INT0_ISR6_R                  0x04    // R
    #define BITEK_MASK_INT0_ISR6_W                  0x05    // W
    #define BITEK_MASK_INT0_ISR6_S                  0x06    // Start
    #define BITEK_MASK_INT0_ISR6_P                  0x07    // Stop

    #define BITEK_MASK_INT1_ISR6_SEL                0x38
    #define BITEK_MASK_INT1_ISR6_IR                 0x00    // IR
    #define BITEK_MASK_INT1_ISR6_SWR                0x08    // S+TX+RX
    #define BITEK_MASK_INT1_ISR6_SR                 0x10    // S+RX
    #define BITEK_MASK_INT1_ISR6_SW                 0x18    // S+TX
    #define BITEK_MASK_INT1_ISR6_R                  0x20    // RX
    #define BITEK_MASK_INT1_ISR6_W                  0x28    // TX
    #define BITEK_MASK_INT1_ISR6_S                  0x30    // Start
    #define BITEK_MASK_INT1_ISR6_P                  0x38    // Stop

    #define BITEK_MASK_INT0_ISR4_SEL                0x40
    #define BITEK_MASK_INT1_ISR4_SEL                0x80

// [1] Clock Domain Systems
#define BITEK_00A_CLK_ATTR0                     0x00A       // [1]
    #define BITEK_MASK_LCLK_SEL                     0x07    // LCLK Clock Domain Source Select
    #define BITEK_MASK_LCLK_POL                     0x08    // LCLK Clock Domain Polarity (0=Normal, 1=Invert)
    #define BITEK_MASK_LCLK_EN                      0x10    // LCLK Clock Domain Enable (0=Disable, 1=Enable)
    #define BITEK_MASK_XCLK_SEL                     0xE0    // XCLK Clock Domain Source Select (XCLK=OSCCLK/2 ^ XCLK_SEL)

#define BITEK_00B_CLK_ATTR1                     0x00B       // [1]
    #define BITEK_MASK_OCLK_SEL                     0x01    // OCLK Clock Output Selcect
    #define BITEK_MASK_OCLK_POL                     0x02    // OCLK Clock Polarity (0=Normal, 1=Invert)
    #define BITEK_MASK_TCLK_PRESEL                  0x0C    // TCLK Pre-division Factor
    #define BITEK_MASK_TCLK_SEL                     0x30    // TCLK Clock Domain Source Select
    #define BITEK_MASK_TCLK_POL                     0x40    // TCLK Clock Domain Polarity
    #define BITEK_MASK_TCLK_EN                      0x80    // TCLK Clock Domain Enable

#define BITEK_00C_CLK_ATTR2                     0x00C       // [1]
    #define BITEK_MASK_MCLK_SEL                     0x03    // MCLK Clock Domain Source Select
    #define BITEK_MASK_MCLK_POL                     0x04    // MCLK Clock Domain Polarity
    #define BITEK_MASK_MCLK_EN                      0x08    // MCLK Clock Domain Enable
    #define BITEK_MASK_PCLK_SEL                     0x30    // PCLK Clock Domain Source Select
    #define BITEK_MASK_PCLK_POL                     0x40    // PCLK Clock Domain Polarity
    #define BITEK_MASK_PCLK_EN                      0x80    // PCLK Clock Domain Enable

#define BITEK_00D_CLK_ATTR3                     0x00D       // [1]
    #define BITEK_MASK_DVPCLK_EN                    0x01    // DVP Clock Domain Enable
    #define BITEK_MASK_DVPCLK_SEL3                  0x02
    #define BITEK_MASK_DVPCLK_POL                   0x04    // DVP Clock Domain Polarity
    #define BITEK_MASK_AFECLK_EN                    0x08    // AFE Clock Domain Enable
    #define BITEK_MASK_AFECLK_SEL                   0x10    // AFE Clock Domain Clock Source Select (0=27MHz, 1=13.5MHz)
    #define BITEK_MASK_AFECLK_POL                   0x20    // AFE Clock Domain Polarity
    #define BITEK_MASK_AFEBUF_SEL                   0x40    // AFE Buffer Clock Domain Clock Source Select (0=DVPCLK, 1=AFECLK))
    #define BITEK_MASK_AFEBUF_POL                   0x80    // AFE Buffer Clock Domain Polarity

#define BITEK_00E_CLK_ATTR4                     0x00E       // [1]
    #define BITEK_MASK_DVPCLK_SEL1                  0x01    // DVP Clock Domain 1 Clock Source Select (0=DVPCLK2, 1=AFE clock)
    #define BITEK_MASK_DVPCLK_SEL2                  0x02    // DVP Clock Domain 2 Clock Source Select (0=PLL, 1=ICLK1)
    #define BITEK_MASK_VDOSCCLK_POL                 0x04
    #define BITEK_MASK_VDOSCCLK_EN                  0x08
    #define BITEK_MASK_BCLK_POL                     0x10
    #define BITEK_MASK_BCLK_SEL                     0x20
    #define BITEK_MASK_OSDCLK_POL                   0x40    // OSD Clock Polarity
    #define BITEK_MASK_OSDCLK_EN                    0x80    // OSD Clock Enable
//====================================================================================================
//===============================SARADC Data Interface============================================
#define BITEK_00F_SARCLK_ATTR                   0x00F
//====================================================================================================
//===================================== M8051 Debug ==================================================
// M8051 Debug
#define BITEK_011012_M8051_BREAK                0x011
#define BITEK_013_M8051_BREAKx                  0x013
#define BITEK_014_M8051_ICACHE                  0x014
#define BITEK_015_M8051_WATCH                   0x015
#define BITEK_016_M8051_ATTR                    0x016
#define BITEK_017_M8051_WDT_ATTR                0x017
//====================================================================================================
//=============================== Panel Timing Setup (OUTPUT)=========================================

#define BITEK_01801B_OS_XP                      0x018       // PANEL HSYNC Pulse Width [1]13
#define BITEK_01901B_OS_XS                      0x019       // PANEL Active Window Horizontal Start Position
#define BITEK_01A01B_OS_XW                      0x01A       // PANEL Active Window Horizontal End   Position
#define BITEK_01B_OS_X_MSB                      0x01B
#define BITEK_01C01E_OS_XT_M0                   0x01C       // PANEL Horizontal Total Length Mode 0
#define BITEK_01D01E_OS_XT_M1                   0x01D       // PANEL Horizontal Total Length Mode 1
#define BITEK_01E_OS_XT_MSB                     0x01E
#define BITEK_01F_OS_YP                         0x01F       // PANEL VSYNC Pulse Width
#define BITEK_020_OS_YS                         0x020       // PANEL Active Window Vertical Start Position
#define BITEK_021023_OS_YW                      0x021       // PANEL Active Window Vertical End   Position
#define BITEK_022023_OS_YT                      0x022       // PANEL Vertical Total Length
// [1] Special Timing Adjustment
#define BITEK_023_ATTR                          0x023
//====================================================================================================
//====================================== Output Data Path ============================================
#define BITEK_024_OUTPUT_ATTR                   0x024       // Output data path
#define BITEK_025_OUTPUT_ATTR1                  0x025
#define BITEK_026_OUTPUT_ATTR2                  0x026
#define BITEK_027_OUTPUT_ATTR3                  0x027
#define BITEK_028_OUTPUT_ATTR4                  0x028
// 0x29: NONE
//====================================================================================================
//===================================== TCON Function ================================================

#define BITEK_02A02C_STH_START                  0x02A       // STH Signal Start[7:0]
#define BITEK_02B02C_STH_END                    0x02B       // STH Signal End[7:0]
#define BITEK_02C_STH_MSB                       0x02C       // STH MSB

#define BITEK_02D02F_LD_START                   0x02D       // LD Signal Start[7:0]
#define BITEK_02E02F_LD_END                     0x02E       // LD Signal End[7:0]
#define BITEK_02F_LD_MSB                        0x02F       // LD MSB
#define BITEK_030032_CKV_START                  0x030       // CKV Signal Start[7:0]
#define BITEK_031032_CKV_END                    0x031       // CKV Signal End[7:0]
#define BITEK_032_CKV_MSB                       0x032       // CKV MSB
#define BITEK_033035_OEH_START                  0x033       // OEH Signal Start[7:0]
#define BITEK_034035_OEH_END                    0x034       // OEH Signal End[7:0]
#define BITEK_035_OEH_MSB                       0x035       // OEH MSB
#define BITEK_036039_VCOM_SHIFT                 0x036       // VCOM Shift
#define BITEK_037039_STV_START                  0x037       // STV Signal Start[7:0]
#define BITEK_038039_STV_END                    0x038       // STV Signal End[7:0]
#define BITEK_039_STV_MSB                       0x039       // STV MSB
#define BITEK_03A_TCON_POL                      0x03A       // TCON Polarity
#define BITEK_03B_TCON_ATTR0                    0x03B       // TCON Attribute0
// [1] TCON Clock Mode
#define BITEK_03C_TCON_ATTR1                    0x03C       // TCON Attribute1
#define BITEK_03D_TCON_ATTR2                    0x03D       // TCON Attribute2
#define BITEK_03E_TCON_ATTR3                    0x03E       // TCON Attribute3
//====================================================================================================
//===================== Background 1 & 2 and Test Pattern Setup ======================================
#define BITEK_03F_R_ATTR                        0x03F
#define BITEK_040_G_ATTR                        0x040
#define BITEK_041_B_ATTR                        0x041
#define BITEK_042_TESTPAT_ATTR                  0x042       // Test Pattern Attribute
// [1] Auto Blank Screen
#define BITEK_043_AUTOON                        0x043       // Auto Blank
#define BITEK_044_AUTOOFF_TIME                  0x044       // No Signal to Blank screen delay
#define BITEK_045_HLCK_SEL                      0x045       // HLCK selection
    #define BITEK_MASK_NOSIG_SEL                    0x08    // Blank Screen Function Signal Selection (0=VP,1=VD)
    #define BITEK_MASK_PATTERN_TYPE                 0x30    // Test Pattern Type (00=Pure Color, 01=Ramp, 10=Grid, 11=Color bar)
    #define BITEK_MASK_PATTERN_SUBTYPE              0xC0    // Test Pattern Direction (0=Decrease, 1=Increae)
//====================================================================================================
//==========================Input Image Window Setup (Input Windows)===================================
#define BITEK_046048_IS_XS_M0                   0x046       // Input Window Horizontal Start Position Mode 0
#define BITEK_047048_IS_XW_M0                   0x047       // Input Window Horizontal End Position   Mode 0
#define BITEK_048_IS_X_M0_MSB                   0x048       // Input Window Horizontal                Mode 0
#define BITEK_04904B_IS_YS_M0                   0x049       // Input Window Vertical Start Position   Mode 0
#define BITEK_04A04B_IS_YW_M0                   0x04A       // Input Window Vertical End Position     Mode 0
#define BITEK_04B_IS_Y_M0_MSB                   0x04B       // Input Window Vertical                  Mode 0
#define BITEK_04C04E_IS_XS_M1                   0x04C       // Input Window Horizontal Start Position Mode 1
#define BITEK_04D04E_IS_XW_M1                   0x04D       // Input Window Horizontal End Position   Mode 1
#define BITEK_04E_IS_X_M1_MSB                   0x04E       // Input Window Horizontal                Mode 1
#define BITEK_04F051_IS_YS_M1                   0x04F       // Input Window Vertical Start Position   Mode 1
#define BITEK_050051_IS_YW_M1                   0x050       // Input Window Vertical End Position     Mode 1
#define BITEK_051_IS_Y_M1_MSB                   0x051       // Input Window Vertical                  Mode 1
//====================================================================================================
//================================ Input Data Path Setup =========================================== 
#define BITEK_052_INPUT_DATAPATH                0x052   // Input Data Path
#define BITEK_MASK_RIN_POL                      0x01    // R Data Input Polarity
#define BITEK_MASK_GIN_POL                      0x02    // G Data Input Polarity
#define BITEK_MASK_BIN_POL                      0x04    // B Data Input Polarity
#define BITEK_MASK_RIN_ROT                      0x08    // R Data Rotation
#define BITEK_MASK_GIN_ROT                      0x10    // G Data Rotation
#define BITEK_MASK_BIN_ROT                      0x20    // B Data Rotation
#define BITEK_MASK_IDE_SEL                      0xC0    // Input Data Enable Source Select

// [1] Input Format
// [1] Input Mode Selection
#define BITEK_053_INPUT_MODE                    0x053
#define BITEK_MASK_IDE_POL                      0x02    // Input Data Enable Source Select
#define BITEK_MASK_IHS_POL                      0x04    // External HS polarity (0=Normal, 1=Invert)
#define BITEK_MASK_IVS_POL                      0x08    // External VS polarity (0=Normal, 1=Invert)
#define BITEK_MASK_EVEN_SEL                     0x30    // EVEN/ODD Signal Selection
#define BITEK_MASK_EXT_SYNC                     0xC0    // Sync source select

#define BITEK_054_INPUT_ATTR0                   0x054       // Input Data Path Attribute
#define BITEK_MASK_ISWAP_RB                     0x01    // Swap R/B Data Bus
#define BITEK_MASK_ISWAP_RG                     0x02    // Swap R/G Data Bus
#define BITEK_MASK_ISWAP_GB                     0x04    // Swap G/B Data Bus
#define BITEK_MASK_VD_PATH                      0x08    // Bus Select (0=External RGB, 1=VD)
#define BITEK_MASK_SORT_656                     0x70    // Input format [1]26
#define BITEK_MASK_VISUAL_TYPE                  0x80    // Visual EVEN/ODD Mode

#define BITEK_055_INPUT_ATTR1                   0x055       // Input Mode Attribute
#define BITEK_MASK_SRC_SEL                      0x03    // Source Format Select
#define BITEK_MASK_SWAP_UV                      0x04    // Swap U and V signal
#define BITEK_MASK_IMODE                        0x08    // Input Mode Select
#define BITEK_MASK_PCLK_BASE                    0x30
//#define BITEK_MASK_PIXEL_MODE                   0x30    // Input Active Pixel Mode
#define BITEK_MASK_RGB2YUV_EN                   0x40    // RGB to YUV color space convert enable
#define BITEK_MASK_RGB2YUV_SEL                  0x80    // RGB to YUV color space convert select
// [1] Auto Switch
// [1] Auto Switch 2
#define BITEK_056_AUOTSWITCH                    0x056
#define BITEK_MASK_AUTO_SWITCH                  0x01    // Auto Switch Mode (0=Manual, 1=Auto)
#define BITEK_MASK_SWITCH_MODE                  0x02    // Switch Mode (0=Mode0, 1=Mode1)
#define BITEK_MASK_AUTO_SWITCH2                 0x04    // Auto Switch Mode2 (0=Manual, 1=Auto)
#define BITEK_MASK_SWITCH_MODE2                 0x08    // Switch Mode2 (0=Mode0, 1=Mode1)
#define BITEK_MASK_AUTOSWITCH_SRC               0x10    // Auto Switch/Switch2 Source Select (0=Mode Type, 1=Video Decoder FIDT)
#define BITEK_MASK_YUV2RGB_EN                   0x40    // YUV to RGB mode enable (0=Gamma, 1=Non-Gamma)
#define BITEK_MASK_Y2R_SEL                      0x80    // YUV to RGB mode select (0=Gamma, 1=Non-Gamma)
//====================================================================================================
//=================================== Display Window Setup ===========================================
#define BITEK_057059_PREDIS_ACTX_S0             0x057       // Display Window Pre-Scaling Active Horizontal Width for Switch2 Mode0
#define BITEK_058059_PREDIS_ACTX_S1             0x058       // Display Window Pre-Scaling Active Horizontal Width for Switch2 Mode1
#define BITEK_059_PREDIS_ACTX_MSB               0x059
#define BITEK_05A05C_DIS_XS_S0                  0x05A       // Display Window Horizontal Start Position for Switch2 Mode0
#define BITEK_05B05C_DIS_XW_S0                  0x05B       // Display Window Horizontal End   Position for Switch2 Mode0
#define BITEK_05D05F_DIS_XS_S1                  0x05D       // Display Window Horizontal Start Position for Switch2 Mode1
#define BITEK_05E05F_DIS_XW_S1                  0x05E       // Display Window Horizontal End   Position for Switch2 Mode1
#define BITEK_05F_DIS_X_S1_MSB                  0x05F
#define BITEK_060062_DIS_YS                     0x060       // Display Window Vertical Start Position for Switch2 Mode1
#define BITEK_061062_DIS_YW                     0x061       // Display Window Vertical End   Position for Switch2 Mode1
#define BITEK_062_DIS_Y_MSB                     0x062
#define BITEK_063065_DIS_XW1                    0x063       // Display Window Horizontal Width
#define BITEK_064065_DIS_XW2                    0x064       // Display Window Horizontal Width Zone 2
#define BITEK_065_DIS_XW_MSB                    0x065       // Display Window Attribute
//====================================================================================================
//=========================== Horizontal & Verical Scaling  ==========================================
#define BITEK_066069_PRESCX_START_S0            0x066       // HSD Start        for Switch2 Mode0
#define BITEK_067069_PRESCX_SHIFT_S0            0x067       // HSD Shift        for Switch2 Mode0
#define BITEK_06806A_PRESCX_FIX_S0              0x068       // HSD Fix          for Switch2 Mode0
#define BITEK_069_PRESCX_S0_MSB                 0x069       // HSD Attribute    for Switch2 Mode0
#define BITEK_06A_PRESCX_S0_ATTR                0x06A       // HSD Attribute                            for Switch2 Mode0
#define BITEK_06B06E_PRESCX_START_S1            0x06B       // HSD Start        for Switch2 Mode1 [1]30
#define BITEK_06C06E_PRESCX_SHIFT_S1            0x06C       // HSD Shift        for Switch2 Mode1
#define BITEK_06D06F_PRESCX_FIX_S1              0x06D       // HSD Fix          for Switch2 Mode1
#define BITEK_06E_PRESCX_S1_MSB                 0x06E       // HSD Attribute    for Switch2 Mode1
#define BITEK_06F_PRESCX_S1_ATTR                0x06F       // HSD Attribute                            for Switch2 Mode1
#define BITEK_070073_SCX1_START_S0              0x070       // HSU Start            for Switch2 Mode0
#define BITEK_071_SCX1_SHIFT_S0                 0x071       // HSU Shift for Zone 1 for Switch2 Mode0
#define BITEK_072073_SCX1_FIX_S0                0x072       // HSU Fix   for Zone 1 for Switch2 Mode0
#define BITEK_073_SCX1_S0_ATTR                  0x073       // HSU Attribute        for Switch2 Mode0
#define BITEK_074077_SCX1_START_S1              0x074       // HSU Start            for Switch2 Mode1
#define BITEK_075_SCX1_SHIFT_S1                 0x075       // HSU Shift for Zone 1 for Switch2 Mode1
#define BITEK_076077_SCX1_FIX_S1                0x076       // HSU Fix   for Zone 1 for Switch2 Mode1
#define BITEK_077_SCX1_S1_ATTR                  0x077       // HSU Attribute        for Switch2 Mode1
#define BITEK_078_SCX2_SHIFT                    0x078       // HSU Shift for Zone 2
#define BITEK_07907C_SCX2_FIX                   0x079       // HSU Fix   for Zone 2
#define BITEK_07A07C_SCX1_INC                   0x07A       // HSU Nonlinear Increase
#define BITEK_07B07C_SCX2_DEC                   0x07B       // HSU Nonlinear Decrease
#define BITEK_07C_SCX_ATTR                      0x07C       // HSU Attribute
#define BITEK_07D_ANZOOM_ATTR                   0x07D       // HSU Attribute
#define BITEK_07E082_SCYE_START_M0              0x07E       // VSX EVEN Field Start for Switch Mode0
#define BITEK_07F082_SCYO_START_M0              0x07F       // VSX ODD  Field Start for Switch Mode0
#define BITEK_080082_SCY_SHIFT_M0               0x080       // VSX Shift            for Switch Mode0
#define BITEK_081082_SCY_FIX_M0                 0x081       // VSX Fix              for Switch Mode0
#define BITEK_082_SCY_M0_MSB                    0x082       // VSX Attribute        for Switch Mode0
#define BITEK_083_SCY_ATTR_M0                   0x083       // VSX Attribute        for Switch Mode0
#define BITEK_084088_SCYE_START_M1              0x084       // VSX EVEN Field Start for Switch Mode1
#define BITEK_085088_SCYO_START_M1              0x085       // VSX ODD  Field Start for Switch Mode1
#define BITEK_086088_SCY_SHIFT_M1               0x086       // VSX Shift            for Switch Mode1
#define BITEK_087088_SCY_FIX_M1                 0x087       // VSX Fix              for Switch Mode1
#define BITEK_088_SCY_M1_MSB                    0x088       // VSX Attribute        for Switch Mode1
#define BITEK_089_VSX_ATTR_M1                   0x089       // VSX Attribute        for Switch Mode1  [1]33
//====================================================================================================
//====================================== Timing Adjustment ===========================================
#define BITEK_08A_MASTER_DLY_M0                 0x08A       // Output VS sysnchorinze Delay time        for Switch Mode0
#define BITEK_08B08D_DLYE_OCLK_M0               0x08B       // Even Field outputs VS delay    in OCLK   for Switch Mode0
#define BITEK_08C08D_DLYO_OCLK_M0               0x08C       // Odd Field outputs VS delay     in OCLK   for Switch Mode0
#define BITEK_08D_DLY_OCLK_M0_MSB               0x08D       // VS delay                                 for Switch Mode0
#define BITEK_08E_MASTER_DLY_M1                 0x08E       // Output VS sysnchorinze Delay time        for Switch Mode1
#define BITEK_08F091_DLYE_OCLK_M1               0x08F       // Even Field outputs VS delay    in OCLK   for Switch Mode1
#define BITEK_090091_DLYO_OCLK_M1               0x090       // Odd Field outputs VS delay     in OCLK   for Switch Mode1
#define BITEK_091_DLY_OCLK_M1_MSB               0x091       // VS delay                                 for Switch Mode1
//====================================================================================================
//====================================ITU656 Encoder Mode===========================================
#define BITEK_092_ITU656_MODE                   0x092
//====================================================================================================
//====================================================================================================
// 0x93 - 0x95: NONE
//====================================================================================================
//=====================================Comb Filter Process (Video Decoder)============================
#define BITEK_096097_COMB_AMPSEL2               0x096
#define BITEK_097_COMB_AMPSEL                   0x097
#define BITEK_098_COMB_LUMA_THD                 0x098   // COMB Y Threshold
#define BITEK_099_COMBY_EN                      0x099   // COMB Y Enable
#define BITEK_09A_COMBC_EN                      0x09A       // COMB C Enable
#define BITEK_09B_COMB_ATTR                     0x09B
//====================================================================================================
//==========================================Luminance Process=========================================
#define BITEK_09C_AMP_P                         0x09C
#define BITEK_09D_AMP_N                         0x09D
#define BITEK_09E_POSTBL_P                      0x09E
#define BITEK_09F_POSTBL_N                      0x09F
#define BITEK_0A0_PREBL_P_LSB                   0x0A0
#define BITEK_0A1_PREBL_N_LSB                   0x0A1
#define BITEK_0A2_PREBL_MSB                     0x0A2
#define BITEK_0A3_PREBL_ATTR                    0x0A3
#define BITEK_0A4_APER_ATTR                     0x0A4
#define BITEK_0A5_Y_ATTR                        0x0A5
//====================================================================================================
//==================================Synchronization Process===========================================
#define BITEK_0A6_SYNC_IDEL                     0x0A6       // Horizontal increment delay
#define BITEK_0A7_SYNC_HSYS                     0x0A7       // Horizontal Sync Start
#define BITEK_0A8_SYNC_HCS                      0x0A8       // Clamp Signal Start
#define BITEK_0A9_SYNC_HSS                      0x0A9       // Horizontal Delay
#define BITEK_0AA_BGPU_POINT_N                  0x0AA       // Burst Gap Start Point for 60Hz signal
#define BITEK_0AB_BGPU_POINT_P                  0x0AB       // Burst Gap Start Point for 50Hz signal
#define BITEK_0AC_SLICER_THD                    0x0AC       // Sync-Slicer Threshold
#define BITEK_0AD_SYNC_ATTR                     0x0AD
#define BITEK_0AE_SYNC_ATTR1                    0x0AE
#define BITEK_0AF0B1_DTO_REF                    0x0AF       // DTO Reference frequency DTO[17:0]
#define BITEK_0AF_DTO_REF07_00                  0x0AF       // DTO Reference frequency DTO[7:0]
#define BITEK_0B0_DTO_REF15_08                  0x0B0       // DTO Reference frequency DTO[15:8]
#define BITEK_0B1_SYNC_ATTR2                    0x0B1
#define BITEK_0B2_SYNC_CDEL07_00                0x0B2        // Horizontal increment delay for chroma CDEL[7:0]
//====================================================================================================
//====================================== Chroma Process ==============================================
#define BITEK_0B3_CHROMA_CBAMP                  0x0B3       // Chroma Gain Cb
#define BITEK_0B4_CHROMA_CRAMP                  0x0B4       // Chroma Gain Cr
#define BITEK_0B5_CHROMA_GAIN_ATTR              0x0B5       // Chroma Gain
#define BITEK_0B60B7_GAIN_CTRL                  0x0B6       // Chroma Gain Reference value
#define BITEK_0B7_CHROMA_ATTR                   0x0B7
#define BITEK_0B8_THRESHOLD_SECAM               0x0B8       // Color Killer Threshold for SECAM
#define BITEK_0B9_THRESHOLD_QAM                 0x0B9       // Color Killer Threshold for PAL and NTSC
#define BITEK_0BA_SECAM_SENSITIVE               0x0BA       // SECAM switch sensitive level
#define BITEK_0BB_PAL_SENSITIVE                 0x0BB       // PAL switch sensitive level
#define BITEK_0BC_STD_OFS00                     0x0BC       // Burst Freq. offset for 3.57MHz
#define BITEK_0BD_STD_OFS01                     0x0BD       // Burst Freq. offset for 4.2 MHz
#define BITEK_0BE_STD_OFS10                     0x0BE       // Burst Freq. offset for 4.43MHz
#define BITEK_0BF_CHROMA_HUE                    0x0BF       // Chroma Hue value
#define BITEK_0C0_CHROMA_ATTR1                  0x0C0
#define BITEK_0C1_STANDARD                      0x0C1
//====================================================================================================
//====================================== AGC and ACC Process =========================================
#define BITEK_0C2_ACLAMP_LEVEL                  0x0C2       // Analog Clamp 1 Level
#define BITEK_0C3_ACLAMP_ATTR0                  0x0C3
#define BITEK_0C4_ACLAMP_ATTR1                  0x0C4
#define BITEK_0C5_ACLAMP_ATTR2                  0x0C5
#define BITEK_0C6_DCLAMPCB_LEVEL                0x0C6
#define BITEK_0C7_DCLAMPCB_ATTR                 0x0C7
#define BITEK_0C8_DCLAMPCR_LEVEL                0x0C8
#define BITEK_0C9_DCLAMPCR_ATTR                 0x0C9
#define BITEK_0CA_DCLAMPY_LEVEL                 0x0CA
#define BITEK_0CB_DCLAMPY_ATTR                  0x0CB
#define BITEK_0CC_DCLAMP_ATTR0                  0x0CC
#define BITEK_0CD_DCLAMP_ATTR1                  0x0CD
#define BITEK_0CE_DGAIN_CB_LEVEL                0x0CE
#define BITEK_0CF_DGAIN_CR_LEVEL                0x0CF
#define BITEK_0D0_DGAIN_ATTR                    0x0D0
#define BITEK_0D1_STABLE_DLY                    0x0D1
#define BITEK_0D2_DGAIN_ATTR1                   0x0D2
#define BITEK_0D3_DGAIN_LEVEL                   0x0D3
#define BITEK_0D4_DGAIN_ATTR2                   0x0D4
#define BITEK_0D5_GAIN_ATTR                     0x0D5
//====================================================================================================
//====================================== VBI Data Slicer =============================================
#define BITEK_0D6_DATA_SLICER_THD               0x0D6       // Data slicer High/Low thresold
#define BITEK_0D7_DATA_SLICER_START             0x0D7       // Data slicer start point
#define BITEK_0D8_DATA_SLICER_E_INFO            0x0D8
#define BITEK_0D9_DATA_SLICER_O_INFO            0x0D9
//====================================================================================================
//========================================Source Detection=============================================
#define BITEK_0DA_AGC_ATTR                      0x0DA
//====================================================================================================
//======================================AFE and PLL Control=============================================
#define BITEK_0DB_AFE_ATTR                      0x0DB
#define BITEK_0DC_AFE_ATTR1                     0x0DC
#define BITEK_0DD_PLL_ATTR                      0x0DD
#define BITEK_0DE_STD_ATTR                      0x0DE   // Very Important 
//====================================================================================================
//======================================Input Path Selection==========================================
#define BITEK_0DF_AFE_ATTR2                     0x0DF
#define BITEK_MASK_FSEL                         0x01    // Manual 50/60Hz select (R_AUFD=0)
#define BITEK_MASK_AUFD                         0x02    // Auto 50/60Hz detection
#define BITEK_MASK_YSRC_SEL                     0x04    // Path select (1=ADC1, 0=ADC2)
#define BITEK_MASK_CSRC_SEL                     0x08    // Path select (1=ADC1, 0=ADC2)
#define BITEK_MASK_YC_EN                        0x10    // Y/C Mode Enable (0=Disable, 1=Enable)
#define BITEK_MASK_YCBCR_EN                     0x20    // YCbCr Mode Enable (0=Disable, 1=Enable)
#define BITEK_MASK_YPBPR_EN                     0x40    // YPbPr Mode Enable (0=Disable, 1=Enable)
#define BITEK_MASK_AFE_SEL                      0x80    // Video mux switch for ADC1 (0=AIN11, 1=AIN12)
#define BITEK_0E0_AFE_ATTR3                     0x0E0
//====================================================================================================
//====================================================================================================
// 0xE5 - 0xE6: NONE
//====================================================================================================
//===================== Interface Speed for OSD, Register0, Register1, XRAM and IRAM =================
#define BITEK_0E7_INF_SPEED                     0x0E7
//====================================================================================================
//==============================Pad Type Setup (Port Controller)======================================
#define BITEK_0E8_RIN_ATTR                      0x0E8
#define BITEK_MASK_RIN10_MFP_EN                 0x01
#define BITEK_MASK_RIN10_MFP_SEL                0x06
#define BITEK_MASK_RIN10_MFP_SEL_IR_TX          0x00
#define BITEK_MASK_RIN10_MFP_SEL_UART_TXD       0x02
#define BITEK_MASK_RIN10_MFP_SEL_I2C_MASTER     0x04
#define BITEK_MASK_RIN10_MFP_SEL_I2C_SLAVE      0x06
#define BITEK_MASK_RIN_SPI_EN                   0x08
#define BITEK_MASK_RIN_GPIO                     0x80    // for P0

#define BITEK_0E9_RIN_PD                        0x0E9       // RIN[7:0] Port Pull-Down resistance on/off
#define BITEK_0EA_RIN_PU                        0x0EA       // RIN[7:0] Port Pull-Up   resistance on/off

#define BITEK_0EB_GIN_ATTR                      0x0EB
#define BITEK_MASK_GIN10_MFP_EN                 0x01
#define BITEK_MASK_GIN10_MFP_SEL                0x06
#define BITEK_MASK_GIN10_MFP_SEL_IR_TX          0x00
#define BITEK_MASK_GIN10_MFP_SEL_UART_TXD       0x02
#define BITEK_MASK_GIN10_MFP_SEL_I2C_MASTER     0x04
#define BITEK_MASK_GIN10_MFP_SEL_I2C_SLAVE      0x06
#define BITEK_MASK_GIN_SPI_EN                   0x08
#define BITEK_MASK_GIN_GPIO                     0x80    // for P1

#define BITEK_0EC_GIN_PD                        0x0EC       // GIN[7:0] Port Pull-Down resistance on/off
#define BITEK_0ED_GIN_PU                        0x0ED       // GIN[7:0] Port Pull-Up   resistance on/off

#define BITEK_0EE_BIN_ATTR                      0x0EE
#define BITEK_MASK_BIN10_MFP_EN                 0x01
#define BITEK_MASK_BIN10_MFP_SEL                0x06
#define BITEK_MASK_BIN10_MFP_SEL_IR_TX          0x00
#define BITEK_MASK_BIN10_MFP_SEL_UART_TXD       0x02
#define BITEK_MASK_BIN10_MFP_SEL_I2C_MASTER     0x04
#define BITEK_MASK_BIN10_MFP_SEL_I2C_SLAVE      0x06
#define BITEK_MASK_BIN_SPI_EN                   0x08
#define BITEK_MASK_BIN_GPIO                     0x80    // for P2

#define BITEK_0EF_BIN_PD                        0x0EF       // BIN[7:0] Port Pull-Down resistance on/off
#define BITEK_0F0_BIN_PU                        0x0F0       // BIN[7:0] Port Pull-Up   resistance on/off

#define BITEK_0F1_VSYNC_ATTR                    0x0F1
#define BITEK_MASK_VSYNC_MFP_EN                 0x01
#define BITEK_MASK_VSYNC_MFP_SEL                0x06
#define BITEK_MASK_VSYNC_MFP_SEL_IR_TX          0x00
#define BITEK_MASK_VSYNC_MFP_SEL_UART_TXD       0x02
#define BITEK_MASK_VSYNC_MFP_SEL_I2C_MASTER     0x04
#define BITEK_MASK_VSYNC_MFP_SEL_I2C_SLAVE      0x06
#define BITEK_MASK_VSYNC_GPIO                   0x08
#define BITEK_MASK_VSYNC_KEY_EN                 0x10
#define BITEK_MASK_VSYNC_PU                     0x20
#define BITEK_MASK_VSYNC_PD                     0x40

#define BITEK_0F2_HSYNC_ATTR                    0x0F2
#define BITEK_MASK_HSYNC_MFP_EN                 0x01
#define BITEK_MASK_HSYNC_MFP_SEL                0x06
//#define BITEK_MASK_HSYNC_MFP_SEL_UART_TXD       0x02
#define BITEK_MASK_HSYNC_MFP_SEL_IR_TX          0x00    // 06:43PM  2013/01/05
#define BITEK_MASK_HSYNC_MFP_SEL_I2C_MASTER     0x04
#define BITEK_MASK_HSYNC_MFP_SEL_I2C_SLAVE      0x06
#define BITEK_MASK_HSYNC_GPIO                   0x08
#define BITEK_MASK_HSYNC_KEY_EN                 0x10
#define BITEK_MASK_HSYNC_PU                     0x20
#define BITEK_MASK_HSYNC_PD                     0x40

#define BITEK_0F3_TOUT_ATTR                     0x0F3
#define BITEK_0F4_TOUT_GPIO                     0x0F4
#define BITEK_0F5_M8051_ATTR1                   0x0F5
#define BITEK_0F6_M8051_ATTR2                   0x0F6
#define BITEK_0F7_M8051_ATTR3                   0x0F7
// [1] GPO Function
#define BITEK_0F8_GPO_REG                       0x0F8       // GPO Value  (0=Low Level, 1=High Level)
#define BITEK_0F9_GPO_TYPE                      0x0F9       // GPO Type   (0=Normal, 1=Tri-State)
#define BITEK_0FA_GPO_SEL                       0x0FA       // GPO Enable (0=Disable, 1=Enable)
//====================================================================================================
//===============================Special Output Setup + Pad Type Setup ===============================
#define BITEK_0FB_RTS0_ATTR                     0x0FB
#define BITEK_MASK_RTS0_GPO_EN                  0x40
#define BITEK_0FC_RTS1_ATTR                     0x0FC
#define BITEK_MASK_RTS1_GPO_EN                  0x40
#define BITEK_0FD_RTS2_ATTR                     0x0FD
#define BITEK_MASK_RTS2_GPO_EN                  0x40
#define BITEK_0FE_PORT_ATTR                     0x0FE   // Port Attribute (1=Tri-state, 0=Normal)
//========================================================================================
//================================= OSD ==================================================
// [1] OSD Function
// OSD0 Window
// [1] OSD Windowss Function
#define BITEK_100_OSD0_X_LSB                    0x100       // OSD0 Start X position
#define BITEK_100105_OSD0_X                     0x100       // OSD0 Start X position

#define BITEK_101_OSD0_Y_LSB                    0x101       // OSD0 Start Y position
#define BITEK_101105_OSD0_Y                     0x101       // OSD0 Start Y position

#define BITEK_102_OSD0_INDEXS_LSB               0x102       // Display RAM Start Index
#define BITEK_102104_OSD0_INDEXS                0x102       // Display RAM Start Index

#define BITEK_103_OSD0_INDEXB_LSB               0x103       // Display RAM Base  Index
#define BITEK_103104_OSD0_INDEXB                0x103       // Display RAM Base  Index

#define BITEK_104_OSD0_INDEX_MSB                0x104
#define BITEK_MASK_OSD0_INDEXB_MSB              0x0F
#define BITEK_MASK_OSD0_INDEXS_MSB              0xF0

#define BITEK_105_OSD0_MSB                      0x105
#define BITEK_MASK_OSD0_X_MSB                   0x07
#define BITEK_MASK_OSD0_Y_MSB                   0x18
#define BITEK_MASK_OSD0_PATSEL                  0x80    // OSD0 Palette Bank select


#define BITEK_106_OSD0_W                        0x106       // OSD0 Width
#define BITEK_107_OSD0_H                        0x107       // OSD0 Height
#define BITEK_MASK_OSD0_HEIGHT                  0x7F

// [1] OSD Windows Attribute
#define BITEK_108_OSD0_MUL                      0x108
#define BITEK_MASK_OSD0_MULX                    0x0F    // OSD0 Horizontal Character Size
#define BITEK_MASK_OSD0_MULY                    0xF0    // OSD0 Vertical Character Size

#define BITEK_109_OSD0_SPC                      0x109
#define BITEK_MASK_OSD0_SPCX                    0x0F    // OSD0 Character Space
#define BITEK_MASK_OSD0_SPCY                    0xF0    // OSD0 Line Space

#define BITEK_10A_OSD0_FRINGE_SEL               0x10A       // OSD0 Font Fringe Selection

#define BITEK_10B_OSD0_ATTR                     0x10B
#define BITEK_MASK_OSD0_FRINGE_COR              0x0F    // OSD0 Window Fringe Color
#define BITEK_MASK_OSD0_FONTSIZE                0x70    // 30h=12bit width


#define BITEK_10C_OSD0_ATTR1                    0x10C
#define BITEK_MASK_OSD0_FFADE_VAL               0x0F    // OSD0 Fade In/Out level
#define BITEK_MASK_OSD0_BFADE_VAL               0xF0    // OSD0 Fade In/Out level

#define BITEK_10D_OSD0_ATTR2                    0x10D
#define BITEK_MASK_OSD0_WX_MIR                  0x01    // OSD0 Window Horizontal Mirror
#define BITEK_MASK_OSD0_WY_MIR                  0x02    // OSD0 Window Vertical Mirror
#define BITEK_MASK_OSD0_FX_MIR                  0x04    // OSD0 Character Horizontal Mirror
#define BITEK_MASK_OSD0_FY_MIR                  0x08    // OSD0 Character Vertical Mirror
#define BITEK_MASK_OSD0_BLINK_SEL               0x70    // OSD0 BLINK
#define BITEK_MASK_OSD0_EN                      0x80    // OSD0 Window Enable

// 0x10E - 0x10F: NONE

// OSD1 Window
#define BITEK_110_OSD1_X_LSB                    0x110       // OSD1 Start X position
#define BITEK_110115_OSD1_X                     0x110       // OSD1 Start X position

#define BITEK_111_OSD1_Y_LSB                    0x111       // OSD1 Start Y position
#define BITEK_111115_OSD1_Y                     0x111       // OSD1 Start Y position

#define BITEK_112_OSD1_INDEXS_LSB               0x112       // Display RAM Start Index
#define BITEK_112114_OSD1_INDEXS                0x112       // Display RAM Start Index

#define BITEK_113_OSD1_INDEXB_LSB               0x113       // Display RAM Base  Index
#define BITEK_113114_OSD1_INDEXB                0x113       // Display RAM Base  Index

#define BITEK_114_OSD1_INDEX_MSB                0x114
    #define BITEK_MASK_OSD1_INDEXB_MSB              0x0F
    #define BITEK_MASK_OSD1_INDEXS_MSB              0xF0

#define BITEK_115_OSD1_MSB                      0x115
    #define BITEK_MASK_OSD1_X_MSB                   0x07
    #define BITEK_MASK_OSD1_Y_MSB                   0x18
    #define BITEK_MASK_OSD1_PATSEL                  0x80    // OSD1 Palette Bank select

#define BITEK_116_OSD1_W                        0x116       // OSD1 Width
#define BITEK_117_OSD1_H                        0x117       // OSD1 Height
    #define BITEK_MASK_OSD1_HEIGHT                  0x7F

// [1] OSD Windows Attribute
#define BITEK_118_OSD1_MUL                      0x118
    #define BITEK_MASK_OSD1_MULX                    0x0F    // OSD1 Horizontal Character Size
    #define BITEK_MASK_OSD1_MULY                    0xF0    // OSD1 Vertical Character Size

#define BITEK_119_OSD1_SPC                      0x119
    #define BITEK_MASK_OSD1_SPCX                    0x0F    // OSD1 Character Space
    #define BITEK_MASK_OSD1_SPCY                    0xF0    // OSD1 Line Space

#define BITEK_11A_OSD1_FRINGE_SEL               0x11A       // OSD1 Font Fringe Selection

#define BITEK_11B_OSD1_ATTR                     0x11B
    #define BITEK_MASK_OSD1_FRINGE_COR              0x0F    // OSD1 Window Fringe Color
    #define BITEK_MASK_OSD1_FONTSIZE                0x70    //


#define BITEK_11C_OSD1_ATTR1                    0x11C
    #define BITEK_MASK_OSD1_FFADE_VAL               0x0F    // OSD1 Fade In/Out level
    #define BITEK_MASK_OSD1_BFADE_VAL               0xF0    // OSD1 Fade In/Out level

#define BITEK_11D_OSD1_ATTR2                    0x11D
    #define BITEK_MASK_OSD1_WX_MIR                  0x01    // OSD1 Window Horizontal Mirror
    #define BITEK_MASK_OSD1_WY_MIR                  0x02    // OSD1 Window Vertical Mirror
    #define BITEK_MASK_OSD1_FX_MIR                  0x04    // OSD1 Character Horizontal Mirror
    #define BITEK_MASK_OSD1_FY_MIR                  0x08    // OSD1 Character Vertical Mirror
    #define BITEK_MASK_OSD1_BLINK_SEL               0x70    // OSD1 BLINK
    #define BITEK_MASK_OSD1_EN                      0x80    // OSD1 Window Enable

// 0x11E - 0x11F: NONE

// OSD2 Window
#define BITEK_120_OSD2_X_LSB                    0x120       // OSD2 Start X position
#define BITEK_120125_OSD2_X                     0x120       // OSD2 Start X position

#define BITEK_121_OSD2_Y_LSB                    0x121       // OSD2 Start Y position
#define BITEK_121125_OSD2_Y                     0x121       // OSD2 Start Y position

#define BITEK_122_OSD2_INDEXS_LSB               0x122       // Display RAM Start Index
#define BITEK_122124_OSD2_INDEXS                0x122       // Display RAM Start Index

#define BITEK_123_OSD2_INDEXB_LSB               0x123       // Display RAM Base  Index
#define BITEK_123124_OSD2_INDEXB                0x123       // Display RAM Base  Index

#define BITEK_124_OSD2_INDEX_MSB                0x124
#define BITEK_MASK_OSD2_INDEXB_MSB              0x0F
#define BITEK_MASK_OSD2_INDEXS_MSB              0xF0

#define BITEK_125_OSD2_MSB                      0x125
#define BITEK_MASK_OSD2_X_MSB                   0x07
#define BITEK_MASK_OSD2_Y_MSB                   0x18
#define BITEK_MASK_OSD2_PATSEL                  0x80    // OSD2 Palette Bank select

#define BITEK_126_OSD2_W                        0x126       // OSD2 Width
#define BITEK_127_OSD2_H                        0x127       // OSD2 Height
#define BITEK_MASK_OSD2_HEIGHT                  0x7F

// [1] OSD Windows Attribute
#define BITEK_128_OSD2_MUL                      0x128
#define BITEK_MASK_OSD2_MULX                    0x0F    // OSD2 Horizontal Character Size
#define BITEK_MASK_OSD2_MULY                    0xF0    // OSD2 Vertical Character Size

#define BITEK_129_OSD2_SPC                      0x129
#define BITEK_MASK_OSD2_SPCX                    0x0F    // OSD2 Character Space
#define BITEK_MASK_OSD2_SPCY                    0xF0    // OSD2 Line Space

#define BITEK_12A_OSD2_FRINGE_SEL               0x12A       // OSD2 Font Fringe Selection

#define BITEK_12B_OSD2_ATTR                     0x12B
#define BITEK_MASK_OSD2_FRINGE_COR              0x0F    // OSD2 Window Fringe Color
#define BITEK_MASK_OSD2_FONTSIZE                0x70    //


#define BITEK_12C_OSD2_ATTR1                    0x12C
#define BITEK_MASK_OSD2_FFADE_VAL               0x0F    // OSD2 Fade In/Out level
#define BITEK_MASK_OSD2_BFADE_VAL               0xF0    // OSD2 Fade In/Out level

#define BITEK_12D_OSD2_ATTR2                    0x12D
#define BITEK_MASK_OSD2_WX_MIR                  0x01    // OSD2 Window Horizontal Mirror
#define BITEK_MASK_OSD2_WY_MIR                  0x02    // OSD2 Window Vertical Mirror
#define BITEK_MASK_OSD2_FX_MIR                  0x04    // OSD2 Character Horizontal Mirror
#define BITEK_MASK_OSD2_FY_MIR                  0x08    // OSD2 Character Vertical Mirror
#define BITEK_MASK_OSD2_BLINK_SEL               0x70    //
#define BITEK_MASK_OSD2_EN                      0x80    // OSD2 Window Enable
//====================================================================================================
//====================================================================================================
// Image Enhancement
#define BITEK_12E_OSDPAT_ATTR                   0x12E
#define BITEK_MASK_OSDPAT_BANK                  0x03    // OSD Palette
#define BITEK_MASK_OSDPAT_SEL                   0x08    //Same as 0x159 bit3
#define BITEK_MASK_OSDPAT_BUSY                  0x80

#define BITEK_MASK_OSDPAT_BANK0                 0x00
#define BITEK_MASK_OSDPAT_BANK1                 0x01
#define BITEK_MASK_OSDPAT_BANK2                 0x02
#define BITEK_MASK_OSDPAT_BANK3                 0x03

#define BITEK_MASK_OSDPAT_R                     0x00    // OSD Palette Color R
#define BITEK_MASK_OSDPAT_G                     0x01    // OSD Palette Color G
#define BITEK_MASK_OSDPAT_B                     0x02    // OSD Palette Color B
#define BITEK_MASK_OSDPAT_A                     0x03    // OSD Palette Attribute

#define BITEK_12F_OSD_ATTR                      0x12F
#define BITEK_MASK_OSD_BANK                     0x0F

#define BITEK_MASK_OSD_BANK0                    0x00
#define BITEK_MASK_OSD_BANK1                    0x01
#define BITEK_MASK_OSD_BANK2                    0x02
#define BITEK_MASK_OSD_BANK3                    0x03
#define BITEK_MASK_OSD_BANK4                    0x04
#define BITEK_MASK_OSD_BANK5                    0x05
#define BITEK_MASK_OSD_BANK6                    0x06
#define BITEK_MASK_OSD_BANK7                    0x07
#define BITEK_MASK_OSD_BANK8                    0x08
#define BITEK_MASK_OSD_BANK9                    0x09
#define BITEK_MASK_OSD_BANK10                   0x0A
#define BITEK_MASK_OSD_BANK11                   0x0B
#define BITEK_MASK_OSD_BANK12                   0x0C
#define BITEK_MASK_OSD_BANK13                   0x0D
#define BITEK_MASK_OSD_BANK14                   0x0E
#define BITEK_MASK_OSD_BANK15                   0x0F
//====================================================================================================
//====================================================================================================
// [1] Post-Processing Brightness/Contrast Adjustment
#define BITEK_130_BRIGHTNESS_R                  0x130       // R Brightness
#define BITEK_131_BRIGHTNESS_G                  0x131       // G Brightness
#define BITEK_132_BRIGHTNESS_B                  0x132       // B Brightness
#define BITEK_133_CONTRAST_R                    0x133       // R Contrast
#define BITEK_134_CONTRAST_G                    0x134       // G Contrast
#define BITEK_135_CONTRAST_B                    0x135       // B Contrast
// [1] Dither
#define BITEK_136_DITHER_L0                     0x136       // Line 0 Dither Factor
#define BITEK_137_DITHER_L1                     0x137       // Line 1 Dither Factor
// [1] LUT Gamma Correction
#define BITEK_138_GAMMA_ATTR                    0x138
// [1] Pre-Processing Brightness/Contrast Adjustment
#define BITEK_139_BLACKLEVEL                    0x139       //
#define BITEK_13A_BRIGHTNESS                    0x13A       // Brightness
#define BITEK_13B_CONTRAST                      0x13B       // Contrast
// [1] Sharpness Process
#define BITEK_13C_UNSHARP_ATTR                  0x13C
#define BITEK_13D_UNSHARP_ATTR1                 0x13D
// [1] Saturation and Kill Color Process
#define BITEK_13E_SAT_U                         0x13E
#define BITEK_13F_SAT_V                         0x13F
#define BITEK_140_KILL_COLOR_ATTR               0x140
// [1] Chroma Transient Improvement (CTI)
#define BITEK_141_CTI                           0x141
#define BITEK_142_CTI_SEL                       0x142
#define BITEK_143_NR_Y                          0x143
#define BITEK_144_NR_UV                         0x144
#define BITEK_145_LOAD                          0x145
//====================================================================================================
//====================================================================================================
// [1] IR Receive
// 0x174  ~ 0x17E  Not Used    
//====================================================================================================
//====================================================================================================
// [1] PWM Function
// 0x17F ~ 0x183 Not Used
//====================================================================================================
//====================================================================================================
// 0x184: NONE
//====================================================================================================
//====================================================================================================
// [1] SPI Transer Function
// 0x185 ~ 0x189
//====================================================================================================
//====================================================================================================
// [1] Timer
#define BITEK_18A_TIMER_ATTR                    0x18A
//====================================================================================================
//====================================================================================================
// 0x18B: NONE
//====================================================================================================
//====================================================================================================
// [1] GPI and KEY Function
// 0x18C ~ 0x197 Not Used
//====================================================================================================
//====================================================================================================
// [1] PLL and OSC Pads
#define BITEK_198_PLL_DM_M0                     0x198       // PLL DM Value
#define BITEK_MASK_PLL_DM_M0                     0x1F

#define BITEK_199_PLL_DN_M0                     0x199       // PLL DN Value
#define BITEK_MASK_PLL_DN_M0                     0x7F

#define BITEK_19A_PLL_DP_M0                     0x19A       // PLL DP Value
#define BITEK_MASK_PLL_DP_M0                     0x3F

#define BITEK_19B_PLL_DM_M1                     0x19B       // PLL DM Value
#define BITEK_MASK_PLL_DM_M1                     0x1F

#define BITEK_19C_PLL_DN_M1                     0x19C       // PLL DN Value
#define BITEK_MASK_PLL_DN_M1                     0x7F

#define BITEK_19D_PLL_DP_M1                     0x19D       // PLL DP Value
#define BITEK_MASK_PLL_DP_M1                     0x3F

#define BITEK_19E_PLL_ATTR1                     0x19E
#define BITEK_MASK_PLL_PD                       0x01    // PLL Power Down Enable (0=Normal, 1=Disable)
#define BITEK_MASK_PLL_RESETN                   0x02    // PLL Reset (0=Reset, 1=Normal)
#define BITEK_MASK_PLL_HALFCLK                  0x04    // Half clock output
#define BITEK_MASK_PLL_SEL                      0x08    // PLL clock control

// PLL_OUT = (R_PLL_DN+1) *2 *1* (OSC_Freq_Sel) / ( (R_PLL_DM+1) * 2^(R_PLL_HALFCK) * ((R_PLL_DP+1)*2)^(R_PLL_SEL)))

//====================================================================================================
//====================================================================================================
// [1] SARADC
#define BITEK_19F_SARADC_ATTR                   0x19F
#define BITEK_1A0_SARADC_ATTR1                  0x1A0
#define BITEK_1A1_SARADC12_SEL                  0x1A1
#define BITEK_1A2_SARADC34_SEL                  0x1A2
#define BITEK_1A3_SARADC_SWITCH                 0x1A3
// [1] ADC Data Interface
#define BITEK_1A4_ADC_COMP_MSB                  0x1A4
#define BITEK_1A5_ADC_COMP_ATTR                 0x1A5
#define BITEK_1A6_ADC_THD                       0x1A6       // ADC value change threshold
#define BITEK_1A7_ADC_ATTR                      0x1A7       //
#define BITEK_1A8_ADC_COMP_ATTR1                 0x1A8
#define BITEK_1A9_ADC_INT_ATTR                  0x1A9
#define BITEK_1AA_SARADC_OUT_I_MSB              0x1AA
#define BITEK_1AB_SARADC_OUT_I_LSB              0x1AB
#define BITEK_1AC_SARADC_INFO                   0x1AC
//====================================================================================================
//====================================================================================================
// [1] I2C Slave
#define BITEK_1AE_I2C_S_MAD                     0x1AE       // I2C Slave MAD Address
#define BITEK_1AF_I2C_S_MASK                    0x1AF       // I2C Slave MAD Mask
#define BITEK_1B0_I2C_S_TBUF                    0x1B0       // I2C Slave Transfer Data
#define BITEK_1B1_I2C_S_ATTR                    0x1B1
#define BITEK_1B2_I2C_S_RBUF_O                  0x1B2       // I2C Slave Receive Data
//====================================================================================================
//====================================================================================================
// [1] I2C Master
#define BITEK_1B3_I2C_M_SPEED                   0x1B3
#define BITEK_1B4_I2C_M_WDATA                   0x1B4
#define BITEK_1B5_I2C_M_ATTR                    0x1B5
#define BITEK_1B6_I2C_M_RDATA_O                 0x1B6
#define BITEK_1B7_I2C_M_INFO                    0x1B7
//====================================================================================================
//====================================================================================================
// 0x1B8: NONE
//====================================================================================================
//====================================================================================================
// I80/M68 Panel
#define BITEK_1B9_CPU_ATTR                      0x1B9
#define BITEK_1BA_CPU_DATAI08_01                0x1BA       // CPU DATAI[08:01]
#define BITEK_1BB_CPU_DATAI17_10                0x1BB       // CPU DATAI[17:10]
#define BITEK_1BC_CPU_ATTR1                     0x1BC
#define BITEK_1BD_CPU_ATTR2                     0x1BD
#define BITEK_1BE_CPU_DATAO08_01_O              0x1BE       // CPU DATAO[08:01]
#define BITEK_1BF_CPU_DATAO17_10_O              0x1BF       // CPU DATAO[17:10]
//====================================================================================================
//====================================================================================================
// Status
#define BITEK_1C0_M8051_BREAK_INFO              0x1C0
#define BITEK_1C1_ITU656_CID_O                  0x1C1
#define BITEK_1C2_IS_XP_O                       0x1C2
#define BITEK_1C31C6_IS_XT_O                    0x1C3
#define BITEK_1C4_IS_YP_O                       0x1C4
#define BITEK_1C51C6_IS_YT_O                    0x1C5
#define BITEK_1C6_IS_T_MSB                      0x1C6
#define BITEK_1C71C9_DE_H_O                     0x1C7
#define BITEK_1C81C9_DE_V_O                     0x1C8
#define BITEK_1C9_DE_MSB_O                      0x1C9
//====================================================================================================
//====================================================================================================
// [1] Auto Detection
#define BITEK_1CA1CC_DET_XP_O                   0x1CA       // HS Low pulse                   in PCLK
#define BITEK_1CB1CC_DET_XN_O                   0x1CB
#define BITEK_1CC_DET_X_MSB_O                   0x1CC
#define BITEK_1CD_DET_STATUS_O                  0x1CD
#define BITEK_1CE1CF_DET_LINEBUF_COUNT_O        0x1CE       // Line buffer count
#define BITEK_1CF_DET_LINEBUF_STATUS_O          0x1CF       // Line buffer status
//====================================================================================================
//====================================================================================================
#define BITEK_1D01D1_CB_DCLAMP_O                0x1D0
#define BITEK_1D0_CB_DCLAMP15_08_O              0x1D0
#define BITEK_1D1_CB_DCLAMP07_00_O              0x1D1

#define BITEK_1D21D3_CR_DCLAMP_O                0x1D2
#define BITEK_1D2_CR_DCLAMP15_08_O              0x1D2
#define BITEK_1D3_CR_DCLAMP07_00_O              0x1D3

#define BITEK_1D41D5_Y_DCLAMP_O                 0x1D4
#define BITEK_1D4_Y_DCLAMP15_08_O               0x1D4
#define BITEK_1D5_Y_DCLAMP07_00_O               0x1D5

#define BITEK_1D61D7_Y_DGAIN_O                  0x1D6
#define BITEK_1D7_GAIN_O                        0x1D7

#define BITEK_1D81D7_LUMA_AMPCOMP_O             0x1D8
#define BITEK_1D8_LUMA_AMPCOMP_LSB_O            0x1D8

#define BITEK_1D9_OVER_COMP_O                   0x1D9

#define BITEK_1DA_INCHRO16_O                    0x1DA

#define BITEK_1DB_INCCHRO15_08_O                0x1DB       // INCCHRO_O[15:8]
#define BITEK_1DC_INCCHRO07_00_O                0x1DC       // INCCHRO_O[ 7:0]

#define BITEK_1DD1DF_STD_GAINOUT_O              0x1DD
#define BITEK_1DE1DF_STD_PHASEOUT_O             0x1DE

#define BITEK_1DF_AFE_INFO_O                    0x1DF


// [1] VBI Data Slicer
#define BITEK_1E0_CC_INFO_O                     0x1E0
#define BITEK_1E1_CC_DATA1_EVEN_O               0x1E1       // Data Slicer First Byte   for EVEN field  [1]48
#define BITEK_1E2_CC_DATA2_EVEN_O               0x1E2       // Data Slicer Second Byte  for EVEN field
#define BITEK_1E3_CC_DATA1_ODD_O                0x1E3       // Data Slicer First Byte   for ODD field
#define BITEK_1E4_CC_DATA2_ODD_O                0x1E4       // Data Slicer Second Byte  for ODD field

#define BITEK_1E5_VD_INFO_O                     0x1E5
    #define BITEK_MASK_VD_READY_O                   0x01
    #define BITEK_MASK_VD_HLCK_O                    0x02    // H-LOCK
    #define BITEK_MASK_SYNC_READY_O                 0x04
    #define BITEK_MASK_STD_READY_O                  0x08
    #define BITEK_MASK_VD_STDCHG_O                  0x10
    #define BITEK_MASK_AUTO_VTRC_O                  0x20    // Auto VTRC mode
		
// [1] Standard Setting and detection
#define BITEK_1E6_STD_INFO_O                    0x1E6
    #define BITEK_MASK_STD_MODE_O                   0x07
    #define BITEK_MASK_FIDT_O                       0x08    // 0=50Hz, 1=60Hz

    #define BITEK_MASK_STD_PAL                      0x00        // 50Hz
    #define BITEK_MASK_STD_PAL_N                    0x01        // 50Hz
    #define BITEK_MASK_STD_SECAM                    0x02        // 50Hz
    #define BITEK_MASK_STD_PAL_M                   (0x03 + 8)   // 60Hz
    #define BITEK_MASK_STD_NTSC443_50               0x04        // 50Hz
    #define BITEK_MASK_STD_NTSC_M                  (0x05 + 8)   // 60Hz
    #define BITEK_MASK_STD_NTSC443_60              (0x06 + 8)   // 60Hz
    #define BITEK_MASK_STD_BLACK_WHITE50            0x07        // 50Hz
    #define BITEK_MASK_STD_BLACK_WHITE60           (0x07 + 8)   // 60Hz
    #define BITEK_MASK_STD_PAL_60                  (0x00 + 8)   // 60Hz

    #define BITEK_MASK_STD_FREQ_O                   0x30
    #define BITEK_MASK_STD_PHASE_O                  0x40
    #define BITEK_MASK_COLORDET_O                   0x80

#define BITEK_1E71EA_Y_ADC_O                    0x1E7           // MSB
#define BITEK_1E81EA_CB_ADC_O                   0x1E8           // MSB
#define BITEK_1E91EA_CR_ADC_O                   0x1E9           // MSB
#define BITEK_1EA_ADC_LSB_O                     0x1EA
//============================================================================
// Memory mapping address
#define BITEK_000_0FF_REGISTER0                 0x000   //For Registers 0x000 ~ 0x0FF
#define BITEK_100_1F7_REGISTER1                 0x100   //For Registers 0x100 ~ 0x1F7
#define BITEK_1F8_1FF_BIT1690                   0x1F8   //For Registers 0x1F8 ~ 0x1FF

#define BITEK_200_2FF_GAMMA                     0x200   // Gamma Table
#define BITEK_300_3FF_OSDPAT                    0x300   // OSD Palette
#define BITEK_400_4FF_OSD_LSB                   0x400   // OSD RAM (Font Code)
#define BITEK_500_5FF_OSD_MSB                   0x500   // OSD RAM (Font Attr)
#define BITEK_400_4FF_UFONT_LSB                 0x400   // OSD User Font LSB
#define BITEK_500_5FF_UFONT_MSB                 0x500   // OSD User Font MSB

#define BITEK_600_6FF_IRAM                      0x600   // IRAM     @Break mode
#define BITEK_700_7FF_XRAM                      0x700   // XRAM     @Break mode

#define BITEK_600_6FF_SFR                       0x600
#define BITEK_700_7FF_ICACHE                    0x700   // ICACHE   @Break mode

#define RAM_8000_9FFF_OSD                       0x8000  // OSD RAM for Master mode ONLY !
//============================================================================================
#define OSD_MIN                 0
#define OSD_DEFAULT             30
#define OSD_MAX                 100
//BRIGHTNESS                0x13A   (VP)
//Y = 2 * (X - 50) + 128, X = 0..100, Default X = 50, Y = 128
//  = 2 * X + 28
//Y = 4 * (X - 15) + 128, X = 0..30, Default X = 15, Y = 128
//  = 4 * X + 68
//Y = 2 * (X - 30) + 128, X = 0..60, Default X = 30, Y = 128
//  = 2 * X + 68
#define BRIGHTNESS_MIN          OSD_MIN
#define BRIGHTNESS_DEFAULT      OSD_DEFAULT
#define BRIGHTNESS_MAX          OSD_MAX

#define BRIGHTNESS_VALUE        128
#define BRIGHTNESS_SLOPE        2
#define BRIGHTNESS_OFFSET       (BRIGHTNESS_VALUE - BRIGHTNESS_SLOPE * BRIGHTNESS_DEFAULT)

//CONTRAST                  0x13B   (VP)
//Y = 2 * (X - 50) + 128, X = 0..100, Default X = 50, Y = 128
//  = 2 * X + 28
//Y = 2 * (X - 30) + 128, X = 0..60, Default X = 30, Y = 128
//  = 2 * X + 68
#define CONTRAST_MIN            OSD_MIN
#define CONTRAST_DEFAULT        OSD_DEFAULT
#define CONTRAST_MAX            OSD_MAX

#define CONTRAST_VALUE          128
#define CONTRAST_SLOPE          2
#define CONTRAST_OFFSET         (CONTRAST_VALUE - CONTRAST_SLOPE * CONTRAST_DEFAULT)

//SHARPNESS                 0x13C   (VP)
//Y = 1 * (X - 50) + 64, X =  0..100,  Default X = 50, Y = 64
//  = 1 * X + 14
//Y = 1 * (X - 50) + 192, X =  0..100,  Default X = 50, Y = 192
//  = 1 * X + 142
//Y = 2 * (X - 30) + 192, X =  0..60,  Default X = 30, Y = 192
//  = 2 * X + 132
#define SHARPNESS_MIN           OSD_MIN
#define SHARPNESS_DEFAULT       OSD_DEFAULT
#define SHARPNESS_MAX           OSD_MAX

#define SHARPNESS_VALUE         192
#define SHARPNESS_SLOPE         2
#define SHARPNESS_OFFSET        (SHARPNESS_VALUE - SHARPNESS_SLOPE * SHARPNESS_DEFAULT)


//SATURATION                0x13E/0x13F (VP)
//Y = 1 * (X - 50) + 64, X =  0..50,  Default X = 50, Y = 64
//  = 1 * X + 14
//Y = 2 * (X - 50) + 64, X = 50..100, Default X = 50, Y = 64
//  = 2 * X - 36
//Y = 2 * (X - 30) + 64, X =  0..60,  Default X = 30, Y = 64
//  = 2 * X + 4
#define SATURATION_MIN          OSD_MIN
#define SATURATION_DEFAULT      OSD_DEFAULT
#define SATURATION_MAX          OSD_MAX

#define SATURATION_VALUE        64
#define SATURATION_SLOPE        2
#define SATURATION_SLOPE2       2
#define SATURATION_OFFSET       (SATURATION_VALUE - SATURATION_SLOPE * SATURATION_DEFAULT)
#define SATURATION_OFFSET2      (SATURATION_SLOPE2 * SATURATION_DEFAULT - SATURATION_VALUE)

#define MSG_SOURCE_NAME_SIZE    10

#define FREERUN_ON()            WriteRegister_8b(BITEK_042_TESTPAT_ATTR, 0xBF) 
#define FREERUN_OFF()           WriteRegister_8b(BITEK_042_TESTPAT_ATTR, 0x3F) 

void BITEK_version(void);

void BITEK_SetBrightness (uint8_t Brightness);
void BITEK_SetContrast (uint8_t Contrast);

void BITEK_TestPattern(_Bool STATE);
uint8_t ReadRegister_8b(uint16_t reg_add);
bool ReadRegister_16b(uint16_t reg_add);
bool ReadRegister_Burst(uint16_t reg_add, uint8_t *read_data,uint8_t len); 
bool WriteRegister_8b(uint16_t reg_add,uint8_t value);
bool WriteRegister_16b(uint16_t reg_add,uint16_t value);
bool WriteRegister_Burst(uint16_t reg_add,uint8_t *array,uint16_t len);
void WriteRegister_Repeat(uint16_t reg_add,uint8_t value,uint8_t count);
void BITEK_Status(void);
bool uDisplay_READ_Burst(uint16_t reg_add,uint8_t len);
void BITEK_OCLK_POL(_Bool POL);
void BITEK_HSYNC_POL(_Bool POL);
void BITEK_VSYNC_POL(_Bool POL);
bool uDisplay_Write_8b(uint16_t reg_add,uint8_t value);
void BITEK_RGB_STATE(_Bool state);
void BITEK_Init1024(void);
_Bool BITEK_DETECT_HZ(void);
_Bool BITEK_SYNC_DETECT(void);
void BITEK_Init_60HZ(void);
void BITEK_Init_50HZ(void);
void BITEK_PWM0_Init(void);
void BITEK_PWM0_UPDATE(uint8_t duty);

