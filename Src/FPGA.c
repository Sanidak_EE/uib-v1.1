#include "FPGA.h"

extern TIM_HandleTypeDef htim2;
extern _Bool   DISPLAY_CMD_RXD;
extern uint8_t DISPLAY_CMD_ID;
extern uint8_t DISPLAY_CMD_VALUE;
uint32_t x1,x2,y1,y2;
_Bool KEY1_First=0;
typedef enum {
              Pressed,	
              Released,
              }Button_Status; 
Button_Status KEY_Status;	
							


typedef enum {
              Short,	
              Long,
              }Press_Type; 
Press_Type Press_Kind;
PRESS_FUNCTION PRESS_FCN;	
extern struct ProcessingUnit_t ProcessingUnit;							
//typedef enum {
//              KEY1_LONG,	
//              KEY1_SHORT,
//	            KEY1_KEY2,
//	            KEY1C_KEY2T,
//	            KEY1_KEY3,
//	            KEY1C_KEY3T,
//	            KEY1_KEY4,
//	            KEY1S_KEY4S,
//              KEY2_LONG,	
//              KEY2_SHORT,	
//	            KEY2_KEY3,
//	            KEY2_KEY4,
//              KEY3_KEY4,	
//              KEY3_SHORT,
//              KEY3_Contr_Bright_UP,	
//	            KEY4_SHORT,
//	            KEY4_Contr_Bright_DOWN,
//	            NO_PRESS,
//              }PRESS_FUNCTION; 
//PRESS_FUNCTION PRESS_FCN;		
					
#define  MAX_BUFFER_SIZE      20

uint8_t  RX_Data=0;
uint8_t  RX_Count=0;
uint8_t  BytesCount=0;
uint8_t  RV_Count=0;
uint8_t  CW_Count=0;
uint8_t  RX_Buffer[MAX_BUFFER_SIZE]={0};
_Bool    RX_Done=0;
_Bool    RX_Error=0;

_Bool    AGC_Mode=0;                       // AGC Mode Auto 0
_Bool    AGC_Manual=0;                       // AGC Mode Manual

_Bool    Contrast_UP=0;
_Bool    Contrast_Brightness=1;           //Contrast/Brightness Flag (=1 Contrast Enabled , =0 Brightness Enabled)
_Bool    Contrast_Down=0;
_Bool    Brightness_UP=0;
_Bool    Brightness_Down=0;

uint8_t  DDE_Level=0;
uint8_t  Image_invert=1;                  // 1=no-operation , 2=Horizontal , 4=Vertical , 8=Diagonal
_Bool    NUC_Mode=1;                      // 1: Auto  , 0=Manual
_Bool    Cursor_Mode=0;                   // 0: Hide  , 1=Display
uint8_t  Cursor_Type=0;                   // from 0 to 11
uint8_t  Pallete_Type=2;                  // from 2 to 11
uint8_t  Zoom_Level=1;                    // from 1x to 4.0x (31 Zoom Levels)
_Bool    Image_Freeze=0;                  // 0:Live  , 1:Freeze
_Bool    White_Hot=1;                     // 0:Black Hot  , 1:White Hot
_Bool    Reticle=0;                       // 0:Cursor OFF  , 1:Cursor ON

typedef enum {
              ProcessStart,	
              BC,
	            OW,
              CW,
              RV,
              SC,
	            ProcessEnd_EB,
	            ProcessEnd_AA,
              }Reply_Message; 
Reply_Message RX_MSG;	
							
		

//======================================= Factory Settings Commands =========================================
//const uint8_t FPGA_FACTORY_SETTINGS[8]={0xAA,0x04,0x00,0x12,0x02,0xC2,0xEB,0xAA};
//======================================= Digital Details Enhancement Commands =========================================
uint8_t       Camera_DDE_Level[9]={0xAA,0x05,0x00,0x3F,0x01,0x00,0xEF,0xEB,0xAA};
//======================================= Image Flip Commands =========================================
uint8_t       Camera_Image_Idle[9]={0xAA,0x05,0x00,0x30,0x01,0x01,0xE1,0xEB,0xAA};
//================================================ NUC Commands =========================================================
//const uint8_t Camera_NUC_Auto[9]    ={0xAA,0x05,0x00,0x15,0x01,0x01,0xC6,0xEB,0xAA};	
//const uint8_t Camera_NUC_Manual[9]  ={0xAA,0x05,0x00,0x15,0x01,0x00,0xC5,0xEB,0xAA};	
//const uint8_t Camera_Shutter_Corr[9]={0xAA,0x05,0x00,0x16,0x01,0x00,0xC6,0xEB,0xAA};
//=========================================Cross Curser Commands ================================================
//const uint8_t Camera_Curser_OFF[9]   ={0xAA,0x05,0x00,0x2B,0x01,0x02,0xDD,0xEB,0xAA};
//const uint8_t Camera_Curser_ON[9]    ={0xAA,0x05,0x00,0x2B,0x01,0x01,0xDC,0xEB,0xAA};
//uint8_t       Camera_Curser_Type[10] ={0xAA,0x06,0x00,0x2B,0x01,0x03,0x00,0xDF,0xEB,0xAA};

//const uint8_t Curser_UP_SHORT[9]     ={0xAA,0x05,0x00,0x2C,0x02,0x06,0xE3,0xEB,0xAA};
//const uint8_t Curser_UP_LONG[9]      ={0xAA,0x05,0x00,0x2C,0x02,0x86,0x63,0xEB,0xAA};

//const uint8_t Curser_DOWN_SHORT[9]   ={0xAA,0x05,0x00,0x2C,0x02,0x07,0xE4,0xEB,0xAA};
//const uint8_t Curser_DOWN_LONG[9]    ={0xAA,0x05,0x00,0x2C,0x02,0x87,0x64,0xEB,0xAA};

//const uint8_t Curser_LEFT_SHORT[9]   ={0xAA,0x05,0x00,0x2C,0x02,0x08,0xE5,0xEB,0xAA};
//const uint8_t Curser_LEFT_LONG[9]    ={0xAA,0x05,0x00,0x2C,0x02,0x88,0x65,0xEB,0xAA};

//const uint8_t Curser_RIGHT_SHORT[9]  ={0xAA,0x05,0x00,0x2C,0x02,0x09,0xE6,0xEB,0xAA};
//const uint8_t Curser_RIGHT_LONG[9]   ={0xAA,0x05,0x00,0x2C,0x02,0x89,0x66,0xEB,0xAA};
//========================================= Palette Commands ================================================
uint8_t       Camera_Palette_Type[9]  ={0xAA,0x05,0x00,0x2D,0x01,0x02,0xDF,0xEB,0xAA};
//=========================================Freeze/Live Commands ================================================
const uint8_t Camera_Image_Freeze[9]={0xAA,0x05,0x00,0x32,0x02,0x01,0xE4,0xEB,0xAA};	
const uint8_t Camera_Image_Live[9]  ={0xAA,0x05,0x00,0x32,0x02,0x00,0xE3,0xEB,0xAA};															
//==================================== Digital Zoom Commands ===========================================================

//==================================================================================================================
const uint8_t  FPGA_Error_Code[9]={0xF2,0xF7,0xF8,0xF9,0xFA,0xFB,0xFD,0xFE,0xFF};

//const uint8_t  Camera_Save_Settings[8]={0xAA,0x04,0x00,0x11,0x01,0xC0,0xEB,0xAA};
//========================================================================
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	  static uint8_t cnt=0;
		if(huart->Instance == USART2)
		{
			  RX_Buffer[RX_Count]=RX_Data;
			  RX_Count++;
//			switch(RX_MSG)
//					{						
//						case ProcessStart:
//								{
//									if(RX_Data == 0x55)
//									  {
//										  RX_Buffer[0]=RX_Data;
//											RX_MSG=BC;
//									  }
//									 else
//										  RX_Error=1;
//									break;
//								}
//						case BC:
//								{
//									BytesCount=RX_Data;
//									RX_Buffer[1]=RX_Data;
//									RV_Count=RX_Data-4;
//									CW_Count=0;
//									RX_MSG=CW;									
//									break;
//								}
//						case CW:
//								{
//									RX_Buffer[2+CW_Count]=RX_Data;
//									CW_Count++;
//									if(CW_Count == 2)
//									  {
//										 CW_Count=0;
//										 RX_MSG=OW;
//										}
//									break;
//								}
//						case OW:
//								{
//									if(RX_Data == 0x33)
//									  {
//										  RX_Buffer[4]=RX_Data;
//											CW_Count=0;
//											cnt=0;
//											RX_MSG=RV;
//									  }
//									 else
//										  RX_Error=1;
//									break;
//								}								
//						case RV:
//								{
//									while(RV_Count)
//									{
//										RV_Count--;
//									  RX_Buffer[5+cnt]=RX_Data;
//										cnt++;
//									}
//									RX_MSG=SC;
//									break;
//								}
//						case SC:
//								{
//									RX_Buffer[BytesCount+1]=RX_Data;
//									RX_MSG=ProcessEnd_EB;
//									break;
//								}
//						case ProcessEnd_EB:
//								{
//									RX_Buffer[BytesCount+2]=RX_Data;
//									if(RX_Data == 0xEB)
//									   RX_MSG=ProcessEnd_AA;
//									else
//										 RX_Error=1;
//									break;
//								}	
//						case ProcessEnd_AA:
//								{
//									RX_Buffer[BytesCount+3]=RX_Data;
//									if(RX_Data == 0xAA)
//									   RX_Done=1;
//                  else
//                    RX_Error=1;										
//									break;
//								}									
//						default:
//									break;								
//		}
	}
//		if(RX_Error !=1 || RX_Done!=1)
		    HAL_UART_Receive_IT(&huart2,&RX_Data,1);
}
//========================================================================
void FPGA_COM_Init(void)
{ 
   MX_USART2_UART_Init();
	 HAL_UART_Receive_IT(&huart2,&RX_Data,1);
	 PRESS_FCN=NO_PRESS;
}
//========================================================================
int FPGA_TX_Command(uint8_t arr[],uint16_t Timeout)	
{ 
	  static uint8_t i=0;
	  static uint16_t count=0;

		memset(RX_Buffer,0,MAX_BUFFER_SIZE);

		for(i=0;i < (arr[1]+4);i++)
			{		 
			   HAL_UART_Transmit(&huart2,&arr[i],1,100);
			}
		HAL_UART_Receive_IT(&huart2,&RX_Data,1);
		RX_Error=0;	
    RX_Done=0;			
		RX_Count=0;
		RX_MSG=ProcessStart;
		
		count=0;
//		while(RX_Done!=1 && RX_Error!=1)
//		{		
//			if( count < (uint16_t)(Timeout/10))
//		  	{
//	    	 HAL_Delay(10);
//				 count++;
//			 }
//			else
//			  break;
//		}

//	if(RX_Done ==1)
//		{
//      //my_printf("RXD Done\r\n");			
//      if( RX_Buffer[2] == 0xFF && RX_Buffer[3] == 0xFF)
//			  {
//					
//          if(
//						 RX_Buffer[2] == Other_Error ||
//						 RX_Buffer[2] == Operation_Word_Error ||
//					   RX_Buffer[2] == command_Support_Read ||
//					   RX_Buffer[2] == command_Support_Write ||
//					   RX_Buffer[2] == command_Support_Setting ||
//					   RX_Buffer[2] == Error_Command ||
//					   RX_Buffer[2] == SC_Error ||
//					   RX_Buffer[2] == Process_End_Error ||
//					   RX_Buffer[2] == Process_Start_Error   
//					  )
//					{
//						//my_printf("RXD with Errors\r\n");
//					  return 0;
//					}
//					else
//						return 1;		
//				}
//		}
//	else
//	 {
//		 my_printf("NoReply RXD\r\n");
//		 return 0;
//	 }
}
//========================================================================================================================
