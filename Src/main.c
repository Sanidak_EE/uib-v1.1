/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "usart.h"
#include "FPGA.h"
#include "gpio.h"
#include "tim.h"
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "MAX17055.h"
#include "BIT1628B.h"
#include "osd.h"
#include <stdlib.h>
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
  uint16_t SLAVES[10]={0};
	uint8_t cnt=0;
	float LTC2941_SOC=0;
	float MAX17055_VBAT=0;
	float MAX17055_SOC=0;
	float MAX17055_CAP=0;
	float MAX17055_Temp=0;
	
	extern uint8_t IDENT;
	extern uint8_t STATUS1;
	extern uint8_t STATUS2;
	extern uint8_t STATUS3;
	_Bool   DISPLAY_CMD_RXD=FALSE;
	uint8_t DISPLAY_CMD_ID=CMD_NONE;
	uint8_t DISPLAY_CMD_VALUE=0;
	
 extern uint8_t        ZOOM_LEVEL;
 extern _Bool          MAINMENU_STATE;
 extern _Bool          SUBMENU_STATE;
 extern _Bool          RSUBMENU_STATE;
 extern _Bool          RETICLEMENU_STATE;
 extern _Bool          VBAR_STATE;
 extern _Bool          DISPLAY_SELECTED;
 extern _Bool          RADJUST_SELECTED;
 extern uint8_t        DISPLAY_SELECTED_IDX;
 extern struct ProcessingUnit_t ProcessingUnit;
	extern uint8_t        OSD0_AREA,OSD1_AREA,OSD2_AREA;
 _Bool KEY1_PRE;
 _Bool KEY2_PRE;
 _Bool KEY3_PRE;
 _Bool KEY4_PRE;
 _Bool KEY1_LONG;
 _Bool KEY2_LONG;
 _Bool KEY3_LONG;
 _Bool KEY4_LONG;
 
 uint8_t counter;
 _Bool cont;
	 
const uint8_t  FPGA_ZOOM_1_0X[17]={0xAA,0x0D,0x00,0x2A,0x01,0x00,0x00,0x00,0x00,0x00,0x7F,0x02,0xFF,0x01,0x63,0xEB,0xAA};
const uint8_t  FPGA_ZOOM_2_0X[17]={0xAA,0x0D,0x00,0x2A,0x01,0x01,0xA0,0x00,0x80,0x00,0xDF,0x01,0x7F,0x01,0x63,0xEB,0xAA};
const uint8_t  FPGA_ZOOM_3_0X[17]={0xAA,0x0D,0x00,0x2A,0x01,0x02,0xD5,0x00,0xAB,0x00,0xA9,0x01,0x54,0x01,0x63,0xEB,0xAA};
const uint8_t  Camera_Zoom_Reply[9]={0x55,0x05,0x00,0x2A,0x33,0x01,0xB8,0xEB,0xAA};

const uint8_t FPGA_WHT_HOT[9]  ={0xAA,0x05,0x00,0x2D,0x01,0x00,0xDD,0xEB,0xAA};
const uint8_t FPGA_BLK_HOT[9]  ={0xAA,0x05,0x00,0x2D,0x01,0x01,0xDE,0xEB,0xAA};
//============================================= Contrast and Brightness(Step)Commands =================================================
uint8_t       FPGA_CONTRAST[10]   ={0xAA,0x06,0x00,0x3B,0x01,0x01,0x05,0xF7,0xEB,0xAA};	      //Set Contrast Up with Step 50 levels                                            
uint8_t       FPGA_BRIGHTNESS[10] ={0xAA,0x06,0x00,0x3C,0x01,0x2C,0x01,0x1A,0xEB,0xAA};       //Set Brightness Up with Step 5 levels
//============================================= AGC Modes ============================================================================
const uint8_t  FPGA_AGC_MANUAL[9]={0xAA,0x05,0x00,0x3A,0x01,0x00,0xEA,0xEB,0xAA};								
const uint8_t  FPGA_AGC_AUTO0[9] ={0xAA,0x05,0x00,0x3A,0x01,0x01,0xEB,0xEB,0xAA};	
const uint8_t  FPGA_AGC_AUTO1[9] ={0xAA,0x05,0x00,0x3A,0x01,0x02,0xEC,0xEB,0xAA};				

const uint8_t FPGA_FACTORY_SETTINGS[8]={0xAA,0x04,0x00,0x12,0x02,0xC2,0xEB,0xAA};

const uint8_t FPGA_RETICLE_OFF[9]   ={0xAA,0x05,0x00,0x2B,0x01,0x02,0xDD,0xEB,0xAA};
const uint8_t FPGA_RETICLE_ON[9]    ={0xAA,0x05,0x00,0x2B,0x01,0x01,0xDC,0xEB,0xAA};
uint8_t       FPGA_RETICLE_TYPE[10] ={0xAA,0x06,0x00,0x2B,0x01,0x03,0x00,0xDF,0xEB,0xAA};

const uint8_t RETICLE_UP_SHORT[9]     ={0xAA,0x05,0x00,0x2C,0x02,0x06,0xE3,0xEB,0xAA};
const uint8_t RETICLE_UP_LONG[9]      ={0xAA,0x05,0x00,0x2C,0x02,0x86,0x63,0xEB,0xAA};
const uint8_t RETICLE_DOWN_SHORT[9]   ={0xAA,0x05,0x00,0x2C,0x02,0x07,0xE4,0xEB,0xAA};
const uint8_t RETICLE_DOWN_LONG[9]    ={0xAA,0x05,0x00,0x2C,0x02,0x87,0x64,0xEB,0xAA};
const uint8_t RETICLE_LEFT_SHORT[9]   ={0xAA,0x05,0x00,0x2C,0x02,0x08,0xE5,0xEB,0xAA};
const uint8_t RETICLE_LEFT_LONG[9]    ={0xAA,0x05,0x00,0x2C,0x02,0x88,0x65,0xEB,0xAA};
const uint8_t RETICLE_RIGHT_SHORT[9]  ={0xAA,0x05,0x00,0x2C,0x02,0x09,0xE6,0xEB,0xAA};
const uint8_t RETICLE_RIGHT_LONG[9]   ={0xAA,0x05,0x00,0x2C,0x02,0x89,0x66,0xEB,0xAA};

      uint8_t FPGA_NUC_AUTO[9]        ={0xAA,0x05,0x00,0x15,0x01,0x01,0xC6,0xEB,0xAA};		
      uint8_t FPGA_NUC_SHUTTER[9]     ={0xAA,0x05,0x00,0x16,0x01,0x00,0xC6,0xEB,0xAA};

const uint8_t FPGA_SAVE_SETTINGS[8]   ={0xAA,0x04,0x00,0x11,0x01,0xC0,0xEB,0xAA};

#define KEY3_PRESSED() !HAL_GPIO_ReadPin(GPIOA, KEY3_UP_Pin)
#define KEY2_PRESSED() !HAL_GPIO_ReadPin(GPIOA, KEY2_DOWN_Pin)
#define KEY4_PRESSED() !HAL_GPIO_ReadPin(GPIOA, KEY4_M_Pin)
#define KEY1_PRESSED() !HAL_GPIO_ReadPin(GPIOA, KEY1_C_Pin)

extern PRESS_FUNCTION PRESS_FCN;
int8_t Mmenu_item=0;
int8_t Rmenu_item=0;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
enum State {stop,waiting,getRegisterAddress,getData,sendData};
typedef struct i2c_slave_init_t i2c_slave_init_t;

struct i2c_slave_init_t
{
  I2C_HandleTypeDef *hi2c;
  void (*error_handler)();
};

void (*_error_handler)();

struct i2c_slave_data_t
{
  enum State transferState;
  uint8_t registerAddress;
  uint8_t receiveBuffer;
  uint8_t i2c_memory[0xFF];
}i2c_data;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void I2C_BatteryMonitor_Scan(void); 
void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c);
void HAL_I2C_ListenCpltCallback(I2C_HandleTypeDef *hi2c);
void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c);
void HAL_I2C_AddrCallback(I2C_HandleTypeDef *hi2c, uint8_t direction, uint16_t addrMatchCode);
void I2C_Slave_Init(void);
void I2C_Slave_DeInit(void);
void I2C_Error_Handler(void);
void I2C_Set_Register(uint8_t reg, uint8_t data);
uint8_t I2C_Get_Register(uint8_t reg);
void SCAN_Push_Buttons(void);
void  DISPLAY_CMD_EXECUTE(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
i2c_slave_init_t I2C_init = {.hi2c = &hi2c2, .error_handler = Error_Handler};
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_I2C2_Init();
	MX_TIM2_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */	
	my_printf("############################################# \r\n"); 
	my_printf("################ UIB v1.1 ################### \r\n"); 	
	FPGA_COM_Init();
	HAL_TIM_Base_Start_IT(&htim2);
  DISPLAY_CMD_ID=CMD_NONE;		
	MAX17055_init();
	
	Power_5V0_ONOFF(ON);        // Power ON Display Driver
	
	HAL_Delay(5000);
	for(uint16_t i=1;i<0x7F;i++) 
		{
			if(HAL_I2C_IsDeviceReady(&hi2c2,i<<1,5,100) == HAL_OK) 
					{
						SLAVES[cnt]=i;
						cnt++;
						my_printf("Slave Address = 0x%2X \r\n",i); 
					} 
			HAL_Delay(5);
		}	
				
	if(OSD_GET_SETTINGS(&ProcessingUnit)==FALSE)
		{
			OSD_RESTORE_DEFAULT(&ProcessingUnit);
      OSD_SAVE_SETTINGS(&ProcessingUnit);
			DISPLAY_CMD_ID=CMD_FACTORY_RESET;	
		}	
	OSD_Init();
	OSD_POLARITY_BATTERY_WSETUP();		
  OSD_POLARITY_UPDATE(WHT_HOT);	
	OSD_BATTERY_UPDATE(16);
	OSD0_ON();
	OSD_AGC_ZOOM_WSETUP(AGCZOOM_WINDOW);
	ProcessingUnit.AGC_MODE=MODE1;
	ProcessingUnit.AGC_TYPE=AGC;
  OSD_ZOOM_UPDATE(AGCZOOM_WINDOW,X1,ProcessingUnit.RETICLE_TYPE);
	OSD_AGC_UPDATE(AGCZOOM_WINDOW,ProcessingUnit.AGC_TYPE,ProcessingUnit.AGC_MODE);
	OSD1_ON();	
	HAL_Delay(200);
  /* USER CODE END 2 */
		
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
      /*USER CODE END WHILE */
		  HAL_Delay(10);
		  SCAN_Push_Buttons();
		  DISPLAY_CMD_EXECUTE();	
      /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLLMUL_4;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLLDIV_2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void I2C_Slave_Init(void)
{
  HAL_I2C_EnableListen_IT(I2C_init.hi2c);
  i2c_data.transferState = waiting;
  _error_handler = I2C_init.error_handler;
}

void I2C_Slave_DeInit(void)
{
	HAL_I2C_DisableListen_IT(I2C_init.hi2c);
  i2c_data.transferState = stop;
  _error_handler = I2C_init.error_handler;
}

void I2C_Error_Handler()
{
  _error_handler();
}

void I2C_Set_Register(uint8_t reg, uint8_t data)
{
  i2c_data.i2c_memory[reg] = data;
}

uint8_t I2C_Get_Register(uint8_t reg)
{
  return i2c_data.i2c_memory[reg];
}

void HAL_I2C_AddrCallback(I2C_HandleTypeDef *hi2c, uint8_t direction, uint16_t addrMatchCode) 
{
	switch(direction) 
		{
			case I2C_DIRECTION_TRANSMIT:
								i2c_data.transferState = getRegisterAddress;
								if(HAL_I2C_Slave_Seq_Receive_IT(hi2c,&i2c_data.receiveBuffer,1,I2C_FIRST_FRAME) != HAL_OK) 
									  {
									    I2C_Error_Handler();
								    }
							  break;
			case I2C_DIRECTION_RECEIVE:
								i2c_data.transferState = sendData;
								if(HAL_I2C_Slave_Seq_Transmit_IT(hi2c,&i2c_data.i2c_memory[i2c_data.registerAddress++],255, I2C_NEXT_FRAME) != HAL_OK) 
									 {
									    I2C_Error_Handler();
								   }
							 break;
			default:
							I2C_Error_Handler();
			break;
	}
}

void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c) 
{
	switch(i2c_data.transferState) 
		{
				case getRegisterAddress:
								i2c_data.registerAddress = i2c_data.receiveBuffer;
								i2c_data.transferState = getData;
								if(HAL_I2C_Slave_Seq_Receive_IT(hi2c,&i2c_data.i2c_memory[i2c_data.registerAddress],1,I2C_FIRST_FRAME)!= HAL_OK) 
									 {
									   I2C_Error_Handler();
								   }
							  break;
				case getData:
					      if(HAL_I2C_Slave_Seq_Receive_IT(hi2c, &i2c_data.i2c_memory[i2c_data.registerAddress], 255, I2C_FIRST_FRAME) != HAL_OK) 
									  {                                            
						          I2C_Error_Handler();
					          }
								 DISPLAY_CMD_RXD   =TRUE;
								 DISPLAY_CMD_ID    =i2c_data.registerAddress;
								 DISPLAY_CMD_VALUE =i2c_data.i2c_memory[i2c_data.registerAddress];
				         break;								 
				default:
					       I2C_Error_Handler();
				break;
	 }
}

void HAL_I2C_ListenCpltCallback(I2C_HandleTypeDef *hi2c) 
{
	HAL_I2C_EnableListen_IT(hi2c);
}

void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c) 
{
	if(HAL_I2C_GetError(hi2c) != HAL_I2C_ERROR_AF) 
		{
		   I2C_Error_Handler();
	  }
}
void I2C_BatteryMonitor_Scan(void) 
{
    for(uint16_t i = 1; i < 0x7F; i++) 
		  {
        if(HAL_I2C_IsDeviceReady(&hi2c2, i<<1,5,100) == HAL_OK) 
					  {
              my_printf("Slave Address = 0x%2X \r\n",i); 
            } 
				HAL_Delay(5);
      }
}

void vprint(const char *fmt, va_list argp) 
{
	char string[200];
	if (0 < vsprintf(string, fmt, argp)) // build string
			{
		    HAL_UART_Transmit(&huart2,(uint8_t*) string,strlen(string),0xffffff); // send message via UART
	    }
}

void my_printf(const char *fmt, ...) // custom printf() function
{
//	va_list argp;
//	va_start(argp, fmt);
//	vprint(fmt, argp);
//	va_end(argp);
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	
	__HAL_TIM_SET_COUNTER(&htim2, 0);   // reset timer 
	counter = 0;                        // reset counter
	cont = 0;                           // rseet continuous flag
	
	switch(GPIO_Pin)
			 {
		     case KEY1_C_Pin:
								if(KEY1_PRESSED())
									 {
											KEY1_PRE = 1;
									 }
								else
									{
											if(KEY1_PRE && !KEY1_LONG)
													{
														// Key1 Short-press Function
														PRESS_FCN=KEY1_SHORT_PRESS;
													}
											 KEY1_PRE = 0;
											 KEY1_LONG = 0;
									 }			
                break;									
		     case KEY2_DOWN_Pin:	
					      
								if(KEY2_PRESSED())
									{  
										KEY2_PRE = 1;
									}
								else
									{
										if(KEY2_PRE && !KEY2_LONG && !KEY4_PRE)
												{
													// Key2 Short-press Function
													PRESS_FCN=KEY2_SHORT_PRESS;
												}
										else if(KEY2_PRE && !KEY2_LONG && KEY4_PRE)
												{
													// KEY2-KEY4 Pressed Together
													PRESS_FCN=KEY2_KEY4_PRESS;						
													KEY4_PRE=0;						
												}
										KEY2_PRE = 0;
										KEY2_LONG = 0;
									}
								 break;		
		     case KEY3_UP_Pin:
								if(KEY3_PRESSED())
									{  
										KEY3_PRE = 1;
									}
								else
									{
										if(KEY3_PRE && !KEY3_LONG)
												{
													// Key3 Short-press Function
													PRESS_FCN=KEY3_SHORT_PRESS;
												}
										KEY3_PRE = 0;
										KEY3_LONG = 0;
									}	
                 break;									
		     case KEY4_M_Pin:
								if(KEY4_PRESSED())
									{										
										KEY4_PRE = 1;
									}
								else
									{
										if(KEY4_PRE && !KEY4_LONG)
												{
													// Key4 Short-press Function
													PRESS_FCN=KEY4_SHORT_PRESS;
												}
										else if(KEY4_PRE && !KEY4_LONG && KEY2_PRE)
												{
													// KEY2-KEY4 Pressed Together
													PRESS_FCN=KEY2_KEY4_PRESS;	
													KEY2_PRE=0;						
												}					
										KEY4_PRE = 0;
										KEY4_LONG = 0;
									}		
                 break;									
					 default:
                  break;	
			}	
}
//==================================================================================================
//when timer period ends, this function will be called
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	counter++;
	// Long-press functions                 
	// Key1
	if(KEY1_PRESSED() && KEY1_PRE && (counter >= 2*10))
			{  
				KEY1_LONG = 1;
				KEY1_PRE = 0;
				// Key1 Long-press Function
				PRESS_FCN=KEY1_LONG_PRESS;	
			}
	// Key3
	if(KEY3_PRESSED() && KEY3_PRE && (counter >= 2*10))
			{ 
				KEY3_LONG = 1;
				KEY3_PRE = 0;
				// Key3 Long-press Function
				PRESS_FCN=KEY3_LONG_PRESS;	
			}
	
	// Continuous-press functions
	// Key2
	if(KEY2_PRESSED() && (counter >= 1*10) && !(KEY4_PRESSED()))
			{ 
				cont=1;
				KEY2_LONG = 1;
				// Key2 Continuous-press Function
				PRESS_FCN=KEY2_CONT_PRESS;		
			}
	// Key4
	if(KEY4_PRESSED() && (counter >= 1*10) && !(KEY2_PRESSED()))
			{ 
				cont=1;
				KEY4_LONG = 1;
				// Key4 Continuous-press Function
				PRESS_FCN=KEY4_CONT_PRESS;	
			}

	if(!(KEY1_PRESSED()) && !(KEY2_PRESSED()) && !(KEY3_PRESSED()) && !(KEY4_PRESSED()) && (counter >= 15*10))
			{
         HAL_TIM_Base_Stop_IT(&htim2);
         counter=0;				
				 if(MAINMENU_STATE && !(SUBMENU_STATE))
					 {
						 MAINMENU_STATE=OFF;
						 OSD_WINDOW_DEFAULT(MAINMENU_WINDOW);													 
					 }
				 else if(SUBMENU_STATE )
					 {
						 SUBMENU_STATE=OFF;
						 OSD_WINDOW_DEFAULT(SUBMENU_WINDOW);
						 MAINMENU_STATE=OFF;
						 OSD_WINDOW_DEFAULT(MAINMENU_WINDOW);														 
					 }
				 else if(RETICLEMENU_STATE && !(RSUBMENU_STATE))	
					 {
						 Rmenu_item=0;
						 RETICLEMENU_STATE=OFF;
						 OSD_WINDOW_DEFAULT(RMENU_WINDOW);
						 OSD_SAVE_SETTINGS(&ProcessingUnit);
             DISPLAY_CMD_ID=CMD_SAVE_SETTINGS;						 
					 }													 
				 else if(RSUBMENU_STATE)
					{
						 Rmenu_item=0;
						 RSUBMENU_STATE=OFF;
						 OSD_WINDOW_DEFAULT(RSUBMENU_WINDOW);	
						 RETICLEMENU_STATE=OFF;
						 OSD_WINDOW_DEFAULT(RMENU_WINDOW);															
					}	
				 else if(VBAR_STATE)
					{
							VBAR_STATE=OFF;	
							OSD_ShowMsg(BATTERY_WINDOW,10,0,(uint8_t*)"   "); 											 														
							OSD_WINDOW_DEFAULT(VBAR_WINDOW);														  
					}													
				 Rmenu_item=0;
				 Mmenu_item=0;														 
				 OSD_AGC_ZOOM_WSETUP(AGCZOOM_WINDOW);
				 OSD_ZOOM_UPDATE(AGCZOOM_WINDOW,ProcessingUnit.ZOOM_LVL,ProcessingUnit.RETICLE_TYPE);	
				 OSD_AGC_UPDATE(AGCZOOM_WINDOW,ProcessingUnit.AGC_TYPE,ProcessingUnit.AGC_MODE);															
				 PRESS_FCN=NO_PRESS; 
         HAL_TIM_Base_Start_IT(&htim2);					
			}			
}
//==================================================================================================
void SCAN_Push_Buttons(void)
{
	switch(PRESS_FCN)
	 {
		case KEY1_SHORT_PRESS:
                    if(!(MAINMENU_STATE || SUBMENU_STATE || RETICLEMENU_STATE || RSUBMENU_STATE))
										  {
											  //Short-cut Mode
												if(ProcessingUnit.ZOOM_LVL==X1)
												  {
													  ProcessingUnit.ZOOM_LVL=X2;
														if(ProcessingUnit.RETICLE_TYPE !=RNONE)
														    ProcessingUnit.RETICLE_TYPE=RWEAPON2;
													}
												else if(ProcessingUnit.ZOOM_LVL==X2)
												  {
													   ProcessingUnit.ZOOM_LVL=X3;
														 if(ProcessingUnit.RETICLE_TYPE !=RNONE)
														   ProcessingUnit.RETICLE_TYPE=RWEAPON3;
													}
												else
												  {
													   ProcessingUnit.ZOOM_LVL=X1;
														 if(ProcessingUnit.RETICLE_TYPE !=RNONE)
														    ProcessingUnit.RETICLE_TYPE=RWEAPON1;
													}
												
												DISPLAY_CMD_ID=CMD_ZOOM_LVL;
                        if(!(VBAR_STATE))												
											      OSD_ZOOM_UPDATE(AGCZOOM_WINDOW,ProcessingUnit.ZOOM_LVL,ProcessingUnit.RETICLE_TYPE);
												else
													  OSD_ZOOM_UPDATE(BATTERY_WINDOW,ProcessingUnit.ZOOM_LVL,ProcessingUnit.RETICLE_TYPE);
											}
	                   else
										  {
											   if(MAINMENU_STATE && !(SUBMENU_STATE))
												   {
													 	 SUBMENU_STATE=OFF;
														 OSD_SHOW_SUBMENU(Mmenu_item,SMENU_UP);
													 }
												 else if(SUBMENU_STATE)
												   {
														 OSD_SHOW_SUBMENU(Mmenu_item,SMENU_SELECT);
													 }													 
                         else if(RETICLEMENU_STATE && !(RSUBMENU_STATE))
												   {
													 	 RSUBMENU_STATE=OFF;
														 OSD_SHOW_RSUBMENU(Rmenu_item,SMENU_UP);
													 }													 
                         else if(RSUBMENU_STATE)
												   {
													   OSD_SHOW_RSUBMENU(Rmenu_item,SMENU_SELECT);
													 }													 
											}
										PRESS_FCN=NO_PRESS;
										break;
		case KEY2_SHORT_PRESS:
                    if(!(MAINMENU_STATE || SUBMENU_STATE || RETICLEMENU_STATE || RSUBMENU_STATE || VBAR_STATE))
										  {
											     //Short-cut Mode
														if(ProcessingUnit.AGC_TYPE==MANUAL)
															{
																OSD_ShowMsg(BATTERY_WINDOW,10,0,(uint8_t*)"   ");
																switch(ProcessingUnit.AGC_MODE)
																		{
																			 case CONTRAST:
                                            OSD_SHOW_VBAR(CONTRAST_BAR,UP);
																			      //DISPLAY_CMD_ID=CMD_CONTRAST;
																						break;
																			 case BRIGHTNESS: 	
                                            OSD_SHOW_VBAR(BRIGHTNESS_BAR,UP);
																			      //DISPLAY_CMD_ID=CMD_BRIGHTNESS;
																			      break;
																			 default:
																						break;																 												
																		}
															}
											}															
	                   else
										  {
											   if(MAINMENU_STATE && !(SUBMENU_STATE))
												    {	
															Mmenu_item++;
															if(Mmenu_item == MMENU_ITEMS)
																Mmenu_item=0;															
													    OSD_SHOW_MAINMENU(Mmenu_item);
														}
												 else if(SUBMENU_STATE )
												    {														
													    OSD_SHOW_SUBMENU(Mmenu_item,SMENU_UP);
														}													 
                         else if(RETICLEMENU_STATE && !(RSUBMENU_STATE))	
												   {
															Rmenu_item++;
															if(Rmenu_item == RMENU_ITEMS)
																Rmenu_item=0;															
													    OSD_SHOW_RETICLE_MENU(Rmenu_item);
													 }														 
                         else if(RSUBMENU_STATE)
												   {
														 OSD_SHOW_RSUBMENU(Rmenu_item,SMENU_UP);
													 }	
												 else if(VBAR_STATE)
														 {
																if(ProcessingUnit.AGC_TYPE==MANUAL)
																	{
																		switch(ProcessingUnit.AGC_MODE)
																				{
																					 case CONTRAST:
																						    DISPLAY_CMD_ID=CMD_CONTRAST;
																								OSD_SHOW_VBAR(CONTRAST_BAR,UP);
																								break;
																					 case BRIGHTNESS: 
   																						  DISPLAY_CMD_ID=CMD_BRIGHTNESS;
																								OSD_SHOW_VBAR(BRIGHTNESS_BAR,UP);
																								break;
																					 default:
																								break;																 												
																				}
																	}
														 }														 
											}		
										PRESS_FCN=NO_PRESS;
										break;	
		case KEY3_SHORT_PRESS:
                    if(!(MAINMENU_STATE || SUBMENU_STATE || RETICLEMENU_STATE || RSUBMENU_STATE || VBAR_STATE))
										  {
											     //Short-cut Mode
														if(ProcessingUnit.AGC_TYPE==AGC)
															{
																DISPLAY_CMD_ID=CMD_AGC_AUTO;
																switch(ProcessingUnit.AGC_MODE)
																		{
																			 case MODE0:
																						ProcessingUnit.AGC_MODE=MODE1;																			      
																						OSD_AGC_UPDATE(AGCZOOM_WINDOW,AGC,MODE1);
																						break;
																			 case MODE1: 	
																						ProcessingUnit.AGC_MODE=MODE0;
																						OSD_AGC_UPDATE(AGCZOOM_WINDOW,AGC,MODE0);
																						break;
                                       /*																			 
																			 case MODE2:
																						ProcessingUnit.AGC_MODE=MODE0;
																						OSD_AGC_UPDATE(AGCZOOM_WINDOW,AGC,MODE0);
																						break;
																			 */
																			 default:
																						break;																 												
																		}
															}
														else
															{
																DISPLAY_CMD_ID=CMD_AGC_MANUAL;
																switch(ProcessingUnit.AGC_MODE)
																		{
																			 case CONTRAST:
																						ProcessingUnit.AGC_MODE=BRIGHTNESS;
																						OSD_AGC_UPDATE(AGCZOOM_WINDOW,MANUAL,BRIGHTNESS);
																						break;
																			 case BRIGHTNESS: 	
																						ProcessingUnit.AGC_MODE=CONTRAST;
																						OSD_AGC_UPDATE(AGCZOOM_WINDOW,MANUAL,CONTRAST);
																			      break;
																			 default:
																						break;																 												
																		}
															}
											}
                     else if(VBAR_STATE)	
										  {
                        OSD_TOGGLE_VBAR();										
											}											 
	                   else
										  {
												 if(SUBMENU_STATE)
												   {
														 if(DISPLAY_SELECTED==ON )
														   {	
																  WriteRegister_Repeat(OSD_ATTAddress(SUBMENU_WINDOW,DISPLAY_SELECTED_IDX,0),SMENU_NM_ATTR,15);	
																	WriteRegister_8b(OSD_DisplayAddress(SUBMENU_WINDOW,DISPLAY_SELECTED_IDX,15-1),FONT_20_BLANK);
                                  WriteRegister_8b(OSD_ATTAddress(SUBMENU_WINDOW,DISPLAY_SELECTED_IDX,15-1),SMENU_NM_ATTR);																	 
															    DISPLAY_SELECTED=OFF;																 
															 }
														 else
                               {																	 
																	 SUBMENU_STATE=OFF;
																	 OSD_WINDOW_DEFAULT(SUBMENU_WINDOW);			
															 }																 
													 }
                         else if(RETICLEMENU_STATE && !(RSUBMENU_STATE))	
												   {
														 Rmenu_item=0;
														 RETICLEMENU_STATE=OFF;
														 MAINMENU_STATE=OFF;
														 OSD_SHOW_MAINMENU(Mmenu_item);	
                             OSD_SAVE_SETTINGS(&ProcessingUnit);
                             DISPLAY_CMD_ID=CMD_SAVE_SETTINGS;														 
													 }													 
                         else if(RSUBMENU_STATE)
												  {
														 if(RADJUST_SELECTED==ON )
														   {
															   RADJUST_SELECTED=OFF;	
																 //===============================================================================
																	WriteRegister_8b(OSD_DisplayAddress(RSUBMENU_WINDOW,0,8),FONT_20_BLANK);
																  WriteRegister_8b(    OSD_ATTAddress(RSUBMENU_WINDOW,0,8),((0 << 4)  | OSD_FONT_FIXED ));	
																  WriteRegister_8b(OSD_DisplayAddress(RSUBMENU_WINDOW,0,ADJUST_WIDTH-1),FONT_20_BLANK);							
																	WriteRegister_8b(    OSD_ATTAddress(RSUBMENU_WINDOW,0,ADJUST_WIDTH-1),((0 << 4)  | OSD_FONT_FIXED ));																		 
																 //===============================================================================
																  WriteRegister_Repeat(OSD_ATTAddress(RSUBMENU_WINDOW,0,0),RSMENU_NM_ATTR,8);
 											            WriteRegister_Repeat(OSD_ATTAddress(RSUBMENU_WINDOW,0,(uint8_t)(0.5*ADJUST_WIDTH+2)),RSMENU_NM_ATTR,8);
															 }
														 else
                               {																	 
																 RSUBMENU_STATE=OFF;
																 OSD_WINDOW_DEFAULT(RSUBMENU_WINDOW);	
																 OSD_SAVE_SETTINGS(&ProcessingUnit);
                                 DISPLAY_CMD_ID=CMD_SAVE_SETTINGS;																 
															 }																														 												
													}	
                         else if(VBAR_STATE)
												  {
													    VBAR_STATE=OFF;	
														  OSD_ShowMsg(BATTERY_WINDOW,10,0,(uint8_t*)"   "); 											 														
			                        OSD_WINDOW_DEFAULT(VBAR_WINDOW);														  
													}
													 
											}		
										PRESS_FCN=NO_PRESS;
										break;
		case KEY4_SHORT_PRESS:
                    if(!(MAINMENU_STATE || SUBMENU_STATE || RETICLEMENU_STATE || RSUBMENU_STATE || VBAR_STATE))
										  {
											     //Short-cut Mode
														if(ProcessingUnit.AGC_TYPE==MANUAL)
															{
																OSD_ShowMsg(BATTERY_WINDOW,10,0,(uint8_t*)"   ");
																switch(ProcessingUnit.AGC_MODE)
																		{																		 
																			 case CONTRAST:
                                            OSD_SHOW_VBAR(CONTRAST_BAR,DOWN);
																			      //DISPLAY_CMD_ID=CMD_CONTRAST;
																						break;
																			 case BRIGHTNESS: 	
                                            OSD_SHOW_VBAR(BRIGHTNESS_BAR,DOWN);
																			      //DISPLAY_CMD_ID=CMD_BRIGHTNESS;
																			      break;
																			 default:
																						break;																 												
																		}
															}
											}															
	                   else
										  {
											   if(MAINMENU_STATE && !(SUBMENU_STATE))
												    {	
															Mmenu_item--;
															if(Mmenu_item <0 )
																Mmenu_item=MMENU_ITEMS-1;															
													    OSD_SHOW_MAINMENU(Mmenu_item);
														}
												 else if(SUBMENU_STATE)
												    {														
													    OSD_SHOW_SUBMENU(Mmenu_item,SMENU_DOWN);
														}													 
                         else if(RETICLEMENU_STATE && !(RSUBMENU_STATE))	
												   {
															Rmenu_item--;
															if(Rmenu_item < 0)
																Rmenu_item=RMENU_ITEMS-1;															
													    OSD_SHOW_RETICLE_MENU(Rmenu_item);
													 }																 
                         else if(RSUBMENU_STATE)
												   {
														 OSD_SHOW_RSUBMENU(Rmenu_item,SMENU_DOWN);
													 }	
												 else if(VBAR_STATE)
														 {
																if(ProcessingUnit.AGC_TYPE==MANUAL)
																	{
																		switch(ProcessingUnit.AGC_MODE)
																				{
																					 case CONTRAST:
																						    DISPLAY_CMD_ID=CMD_CONTRAST;
																								OSD_SHOW_VBAR(CONTRAST_BAR,DOWN);
																								break;
																					 case BRIGHTNESS: 
    																						DISPLAY_CMD_ID=CMD_BRIGHTNESS; 
																								OSD_SHOW_VBAR(BRIGHTNESS_BAR,DOWN);
																								break;
																					 default:
																								break;																 												
																				}
																	}
														 }													 
											}		
										PRESS_FCN=NO_PRESS;
										break;	
		case KEY1_LONG_PRESS:
									  OSD_ZOOM_UPDATE(BATTERY_WINDOW,ProcessingUnit.ZOOM_LVL,ProcessingUnit.RETICLE_TYPE);	
									  OSD_AGC_UPDATE(BATTERY_WINDOW,ProcessingUnit.AGC_TYPE,ProcessingUnit.AGC_MODE);
                    Mmenu_item=0;		
                    OSD_SHOW_MAINMENU(Mmenu_item);
										PRESS_FCN=NO_PRESS;
										break;	
		case KEY3_LONG_PRESS:
                    if(!(MAINMENU_STATE || SUBMENU_STATE || RETICLEMENU_STATE || RSUBMENU_STATE || VBAR_STATE))
										  {
											  //Short-cut Mode
												if(ProcessingUnit.AGC_TYPE==AGC)
											  	{
														DISPLAY_CMD_ID=CMD_AGC_MANUAL;
												  	ProcessingUnit.AGC_TYPE=MANUAL;
													}
												else
												  {
													  DISPLAY_CMD_ID=CMD_AGC_AUTO;
													  ProcessingUnit.AGC_TYPE=AGC;
												  }
                        OSD_AGC_UPDATE(AGCZOOM_WINDOW,ProcessingUnit.AGC_TYPE,ProcessingUnit.AGC_MODE);
											}
	                   else
										  {
											   if(MAINMENU_STATE && !(SUBMENU_STATE))
												   {
                             MAINMENU_STATE=OFF;
														 OSD_WINDOW_DEFAULT(MAINMENU_WINDOW);													 
													 }
												 else if(SUBMENU_STATE )
												   {
														 SUBMENU_STATE=OFF;
														 OSD_WINDOW_DEFAULT(SUBMENU_WINDOW);
                             MAINMENU_STATE=OFF;
                             OSD_WINDOW_DEFAULT(MAINMENU_WINDOW);														 
													 }
                         else if(RETICLEMENU_STATE && !(RSUBMENU_STATE))	
												   {
														 Rmenu_item=0;
														 RETICLEMENU_STATE=OFF;
												     OSD_WINDOW_DEFAULT(RMENU_WINDOW);
                             OSD_SAVE_SETTINGS(&ProcessingUnit);
                             DISPLAY_CMD_ID=CMD_SAVE_SETTINGS;														 
													 }													 
                         else if(RSUBMENU_STATE)
												  {
														 Rmenu_item=0;
														 RSUBMENU_STATE=OFF;
														 OSD_WINDOW_DEFAULT(RSUBMENU_WINDOW);	
														 RETICLEMENU_STATE=OFF;
												     OSD_WINDOW_DEFAULT(RMENU_WINDOW);
                             OSD_SAVE_SETTINGS(&ProcessingUnit);
                             DISPLAY_CMD_ID=CMD_SAVE_SETTINGS;														
													}	
                         else if(VBAR_STATE)
												  {
													    VBAR_STATE=OFF;	
														  OSD_ShowMsg(BATTERY_WINDOW,10,0,(uint8_t*)"   "); 											 														
			                        OSD_WINDOW_DEFAULT(VBAR_WINDOW);														  
													}													
												 Rmenu_item=0;
												 Mmenu_item=0;														 
												 OSD_AGC_ZOOM_WSETUP(AGCZOOM_WINDOW);
												 OSD_ZOOM_UPDATE(AGCZOOM_WINDOW,ProcessingUnit.ZOOM_LVL,ProcessingUnit.RETICLE_TYPE);	
												 OSD_AGC_UPDATE(AGCZOOM_WINDOW,ProcessingUnit.AGC_TYPE,ProcessingUnit.AGC_MODE);														
											}		
										PRESS_FCN=NO_PRESS;
										break;
		case KEY2_CONT_PRESS:
                    if(!(MAINMENU_STATE || SUBMENU_STATE || RETICLEMENU_STATE || RSUBMENU_STATE || VBAR_STATE))
										   {
											     //Short-cut Mode
														if(ProcessingUnit.AGC_TYPE==MANUAL)
															{
																OSD_ShowMsg(BATTERY_WINDOW,10,0,(uint8_t*)"   ");
																switch(ProcessingUnit.AGC_MODE)
																		{
																			 case CONTRAST:
                                            OSD_SHOW_VBAR(CONTRAST_BAR,UP);
																						break;
																			 case BRIGHTNESS: 	
                                            OSD_SHOW_VBAR(BRIGHTNESS_BAR,UP);
																			      break;
																			 default:
																						break;																 												
																		}
															}
											 }					
                    else if(VBAR_STATE && (RADJUST_SELECTED==OFF))
										   {
														if(ProcessingUnit.AGC_TYPE==MANUAL)
															{
																switch(ProcessingUnit.AGC_MODE)
																		{																		 
																			 case CONTRAST:
																				    DISPLAY_CMD_ID=CMD_CONTRAST;	
                                            OSD_SHOW_VBAR(CONTRAST_BAR,UP);
																						break;
																			 case BRIGHTNESS: 
                                            DISPLAY_CMD_ID=CMD_BRIGHTNESS;																					 
                                            OSD_SHOW_VBAR(BRIGHTNESS_BAR,UP);
																			      break;
																			 default:
																						break;																 												
																		}
															}
														HAL_Delay(100);
											 }            											 
                     else if(!(VBAR_STATE) && (RADJUST_SELECTED==ON))
										   {
                          OSD_SHOW_RSUBMENU(Rmenu_item,SMENU_UP);
													HAL_Delay(100);
											 }  											 
										PRESS_FCN=NO_PRESS;
										break;		
		case KEY4_CONT_PRESS:
                    if(!(MAINMENU_STATE || SUBMENU_STATE || RETICLEMENU_STATE || RSUBMENU_STATE || VBAR_STATE))
										   {
											     //Short-cut Mode
														if(ProcessingUnit.AGC_TYPE==MANUAL)
															{
																OSD_ShowMsg(BATTERY_WINDOW,10,0,(uint8_t*)"   ");
																switch(ProcessingUnit.AGC_MODE)
																		{
																			 case CONTRAST:
                                            OSD_SHOW_VBAR(CONTRAST_BAR,DOWN);
																						break;
																			 case BRIGHTNESS: 	
                                            OSD_SHOW_VBAR(BRIGHTNESS_BAR,DOWN);
																			      break;
																			 default:
																						break;																 												
																		}
															}
											 }					
                    else if(VBAR_STATE && (RADJUST_SELECTED==OFF))			
										   {
															if(ProcessingUnit.AGC_TYPE==MANUAL)
															{
																//OSD_ShowMsg(BATTERY_WINDOW,10,0,(uint8_t*)"   ");
																switch(ProcessingUnit.AGC_MODE)
																		{																		 
																			 case CONTRAST:
																				    DISPLAY_CMD_ID=CMD_CONTRAST;
                                            OSD_SHOW_VBAR(CONTRAST_BAR,DOWN);
																						break;
																			 case BRIGHTNESS:
                                            DISPLAY_CMD_ID=CMD_BRIGHTNESS;																				 
                                            OSD_SHOW_VBAR(BRIGHTNESS_BAR,DOWN);
																			      break;
																			 default:
																						break;																 												
																		}
															}
														HAL_Delay(100);
											 }
                     else if(!(VBAR_STATE) && (RADJUST_SELECTED==ON))
										   {
                          OSD_SHOW_RSUBMENU(Rmenu_item,SMENU_DOWN);
													HAL_Delay(100);
											 }                       											 
										PRESS_FCN=NO_PRESS;
										break;	
		case KEY2_KEY4_PRESS:
                    if(!(MAINMENU_STATE || SUBMENU_STATE || RETICLEMENU_STATE || RSUBMENU_STATE ))
										   {
                          if(ProcessingUnit.POLARITY == BLK_HOT)
														 ProcessingUnit.POLARITY = WHT_HOT;
													else
														 ProcessingUnit.POLARITY = BLK_HOT;
													
													DISPLAY_CMD_ID=CMD_POLARITY;
													OSD_POLARITY_UPDATE(ProcessingUnit.POLARITY);														
											 }                       											 
										PRESS_FCN=NO_PRESS;
										break;												 
		case NO_PRESS:

										break;					
						 default:
										break;
	 }
}

//========================================================================================================================
void  DISPLAY_CMD_EXECUTE(void)
{
	    static uint8_t temp=0;
		  static uint8_t buf[3]={0};
    	switch(DISPLAY_CMD_ID)
	     {
				case CMD_ZOOM_LVL:
															switch(ProcessingUnit.ZOOM_LVL)
																		{
																			 case 0:
																							 FPGA_TX_Command((uint8_t *)FPGA_ZOOM_1_0X,1000);	
																							 break;
																			 case 1:	
																							 FPGA_TX_Command((uint8_t *)FPGA_ZOOM_2_0X,1000);	
																							 break;
																			 case 2:	
																							 FPGA_TX_Command((uint8_t *)FPGA_ZOOM_3_0X,1000);	
																							 break;																 
																			 default:
																							 break;
																		
																		}	
														 HAL_Delay(10);				
														 if(ProcessingUnit.RETICLE_TYPE == RNONE)
															 {
																 FPGA_TX_Command((uint8_t *)FPGA_RETICLE_OFF,1000);	
															 }
														 else
															 {															
																 FPGA_RETICLE_TYPE[6]=ProcessingUnit.RETICLE_TYPE;
																 FPGA_RETICLE_TYPE[7]=0xDF+ProcessingUnit.RETICLE_TYPE;
																 FPGA_TX_Command((uint8_t *)FPGA_RETICLE_TYPE,1000);																
															 }																				
                               break;							
				case CMD_POLARITY:
														if(ProcessingUnit.POLARITY==BLK_HOT)
														  {
															  FPGA_TX_Command((uint8_t *)FPGA_BLK_HOT,1000);	
														  }
														 else
														  {
															  FPGA_TX_Command((uint8_t *)FPGA_WHT_HOT,1000);													 
														   }
                             break;																																				
						
				case CMD_BRIGHTNESS:
														FPGA_BRIGHTNESS[5]=(uint8_t)((ProcessingUnit.BRIGHT_LVL)*0.01*511) & 0xFF;
														FPGA_BRIGHTNESS[6]=((uint16_t)((ProcessingUnit.BRIGHT_LVL)*0.01*511)>>8) & 0xFF;
					                    for(char i=0; i<FPGA_BRIGHTNESS[1]+1; i++)
				                        temp+=FPGA_BRIGHTNESS[i];
				                    FPGA_BRIGHTNESS[7]=FPGA_BRIGHTNESS[0]+FPGA_BRIGHTNESS[1]+FPGA_BRIGHTNESS[2]+FPGA_BRIGHTNESS[3]+FPGA_BRIGHTNESS[4]+FPGA_BRIGHTNESS[5]+FPGA_BRIGHTNESS[6];
														FPGA_TX_Command((uint8_t *)FPGA_BRIGHTNESS,1000);															
                            break;				
				case CMD_CONTRAST:
														FPGA_CONTRAST[5]=(uint8_t)((ProcessingUnit.CONTRAST_LVL)*0.01*255) & 0xFF;
														FPGA_CONTRAST[6]=((uint16_t)((ProcessingUnit.CONTRAST_LVL)*0.01*255)>>8) & 0xFF;
					                    for(char i=0; i<FPGA_CONTRAST[1]+1; i++)
				                        temp+=FPGA_CONTRAST[i];
				                    FPGA_CONTRAST[7]=FPGA_CONTRAST[0]+FPGA_CONTRAST[1]+FPGA_CONTRAST[2]+FPGA_CONTRAST[3]+FPGA_CONTRAST[4]+FPGA_CONTRAST[5]+FPGA_CONTRAST[6];
														FPGA_TX_Command((uint8_t *)FPGA_CONTRAST,1000);														
				                    break;
				case CMD_AGC_AUTO:
					                 if(ProcessingUnit.AGC_MODE == MODE0)
													     FPGA_TX_Command((uint8_t *)FPGA_AGC_AUTO0,1000);
													 else
													     FPGA_TX_Command((uint8_t *)FPGA_AGC_AUTO1,1000);
                           break;			
				case CMD_AGC_MANUAL:
													 FPGA_TX_Command((uint8_t *)FPGA_AGC_MANUAL,1000);	
													 break;
				case CMD_DISPLAY_LUM:
					                 buf[0]=0xAA;
				                   buf[1]=ProcessingUnit.DISPLAY_LUM_LVL;
													 HAL_I2C_Master_Transmit(&hi2c2,uDISPLAY_ADD,buf,2,100);
													 break;					
				case CMD_CAL_TYPE:
                           if(ProcessingUnit.CAL==0)
													    {
															  FPGA_NUC_AUTO[5]=0x01;
																FPGA_NUC_AUTO[6]=0xC6;
															  FPGA_TX_Command((uint8_t *)FPGA_NUC_AUTO,1000);
															}		
                           else if(ProcessingUnit.CAL==1)	
													    {
																//Shutter = External CAL
															  FPGA_NUC_SHUTTER[5]=0x00;
																FPGA_NUC_SHUTTER[6]=0xC6;																
															  FPGA_TX_Command((uint8_t *)FPGA_NUC_SHUTTER,1000);
															}	
                           else
													    {
																//Background = Manual CAL
															  FPGA_NUC_SHUTTER[5]=0x02;
																FPGA_NUC_SHUTTER[6]=0xC8;
															  FPGA_TX_Command((uint8_t *)FPGA_NUC_SHUTTER,1000);
															}														 
													 break;					
				case CMD_RETICLE_TYPE:
					                 if(ProcessingUnit.RETICLE_TYPE == RNONE)
													   {
														   FPGA_TX_Command((uint8_t *)FPGA_RETICLE_OFF,1000);	
														 }
													 else
														 {															
															 FPGA_RETICLE_TYPE[6]=ProcessingUnit.RETICLE_TYPE;
															 FPGA_RETICLE_TYPE[7]=0xDF+ProcessingUnit.RETICLE_TYPE;
															 FPGA_TX_Command((uint8_t *)FPGA_RETICLE_TYPE,1000);																
														 }		
														break;
				case CMD_ELV_UP:											    
				                  FPGA_TX_Command((uint8_t *)RETICLE_UP_SHORT,1000);		
                          break;
				case CMD_ELV_DOWN:											    
				                  FPGA_TX_Command((uint8_t *)RETICLE_DOWN_SHORT,1000);		
                          break;	
				case CMD_WND_RIGHT:											    
				                  FPGA_TX_Command((uint8_t *)RETICLE_RIGHT_SHORT,1000);		
                          break;	
				case CMD_WND_LEFT:											    
				                  FPGA_TX_Command((uint8_t *)RETICLE_LEFT_SHORT,1000);		
                          break;
				case CMD_SAVE_SETTINGS:											    
				                  FPGA_TX_Command((uint8_t *)FPGA_SAVE_SETTINGS,1000);		
                          break;		
				case CMD_FACTORY_RESET:											    
				                  FPGA_TX_Command((uint8_t *)FPGA_FACTORY_SETTINGS,1000);			
                          break;					
			  default        :
					              break;
			 }
		DISPLAY_CMD_ID=CMD_NONE;
}

void RETICLE_READJUST(int16_t wnd_shift,int16_t elv_shift)
{
	 if(ProcessingUnit.RETICLE_TYPE == RNONE)
		 {
			 FPGA_TX_Command((uint8_t *)FPGA_RETICLE_OFF,1000);	
		 }
	 else
		 {															
			 FPGA_RETICLE_TYPE[6]=ProcessingUnit.RETICLE_TYPE;
			 FPGA_RETICLE_TYPE[7]=0xDF+ProcessingUnit.RETICLE_TYPE;
			 FPGA_TX_Command((uint8_t *)FPGA_RETICLE_TYPE,1000);																
		 }		
	HAL_Delay(5);
		 
	if(wnd_shift<0)
	  {
			for(char i=0; i< abs(wnd_shift); i++)       
		    {
		      FPGA_TX_Command((uint8_t *)RETICLE_LEFT_SHORT,1000);
					HAL_Delay(5);
				}					
		}
	else
	  {
			for(char i=0; i< abs(wnd_shift); i++)       
		    {			
		      FPGA_TX_Command((uint8_t *)RETICLE_RIGHT_SHORT,1000);	
					HAL_Delay(5);
				}
		}
		
	if(elv_shift<0)
	  {
			for(char i=0; i< abs(elv_shift); i++)       
		    {			
		      FPGA_TX_Command((uint8_t *)RETICLE_DOWN_SHORT,1000);	
					HAL_Delay(5);					
				}
		}
	else
	  {
			for(char i=0; i< abs(elv_shift); i++)       
		    {			
		      FPGA_TX_Command((uint8_t *)RETICLE_UP_SHORT,1000);	
					HAL_Delay(5);					
				}
		}
		
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
