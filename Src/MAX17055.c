#include "MAX17055.h"

extern   I2C_HandleTypeDef hi2c2;
uint16_t temp_read,Status,StatusPOR,HibCFG=0;	
//==========================================================================
uint16_t ReadRegister(uint8_t reg) 
{
	uint16_t MSB = 0;
	uint16_t LSB = 0;
	uint8_t buf[3] = {reg};
	
	if(HAL_I2C_Master_Transmit(&hi2c2,MAX17055_ADDRESS,buf,1,100) == HAL_OK)  
	   {
		    if(HAL_I2C_Master_Receive(&hi2c2,MAX17055_ADDRESS,buf,2,100) == 	HAL_OK)	
				  {					
						LSB=buf[0];
						MSB=buf[1];
					  return( (MSB << 8 & 0xFF00) | (LSB & 0x00FF));	
					}
     }
	 else
		 return 0;
}
//==========================================================================
_Bool WriteRegister(uint8_t reg,uint16_t value)
{
	uint8_t MSB = 0;
	uint8_t LSB = 0;
 	MSB = (value >> 8) & 0x00FF;
	LSB = value & 0x00FF;
	uint8_t buf[3] = {reg,LSB,MSB};
	return (HAL_I2C_Master_Transmit(&hi2c2,MAX17055_ADDRESS,buf,3,100));
}
//==========================================================================
void MAX17055_init() 
	{
    StatusPOR = ReadRegister(0x0000);
    //WriteRegister(0x0000,  StatusPOR & 0xFFFD);
		 WriteRegister(0x0000, 0x0002);

    // Store Hibernate Configuration
    uint16_t hibCfg = ReadRegister(MAX17055_REG_CONFIG_HipCfg);
    // Exit Hibernate mode
    WriteRegister(0x60, 0x0090);
    WriteRegister(0xBA, 0x0000);
    WriteRegister(0x60, 0x0000);

	WriteRegister(MAX17055_REG_CONFIG_DesignCap, DesignCap);
	WriteRegister(MAX17055_REG_CONFIG_IChgTerm, IChgTerm);
	WriteRegister(MAX17055_REG_CONFIG_VEmpty, VEmpty);
	
  WriteRegister (0x45 , DesignCap*0.03125);                    // Write dQAcc		
	WriteRegister (0x46 , DesignCap*0.03125*44138/DesignCap);    // Write dPAcc	
	WriteRegister (0xDB , 0x8000) ;                              // Write ModelCFG ModelID=0 and VCH=4.2	
	
  WriteRegister(MAX17055_REG_Status_Reset, MAX17055_Status_Reset_POR);		
  WriteRegister(MAX17055_REG_CONFIG_HipCfg, HibCFG);

  uint16_t model = ReadRegister(0xDB);
	HAL_Delay(1000);
	//===============================================
//	   my_printf("%d  \r\n",ReadRegister(0x0000));
//	   my_printf("%d  \r\n",ReadRegister(MAX17055_REG_CONFIG_HipCfg));
//	   my_printf("%d  \r\n",ReadRegister(MAX17055_REG_CONFIG_DesignCap));
//	   my_printf("%d  \r\n",ReadRegister(MAX17055_REG_CONFIG_IChgTerm));
//	   my_printf("%d  \r\n",ReadRegister(MAX17055_REG_CONFIG_VEmpty));
//	   my_printf("%d  \r\n",ReadRegister(0x45));
//	   my_printf("%d  \r\n",ReadRegister(0x46));
//	   my_printf("%d  \r\n",ReadRegister(0xDB));		 
	//===============================================
}
//==========================================================================
/* @brief  Gets the current value in mA */
float GetCurrent_mA() 
{
  int16_t valueDec = ReadRegister(MAX17055_REG_CURRENT);
  return (valueDec * 0.0015625 /SenseRes);
}

//==========================================================================
/* Gets the Average current value in mA*/
float GetAvgCurrent_mA() 
{
  int16_t valueDec =ReadRegister(MAX17055_REG_AVGCURRENT);
  return (valueDec * 0.0015625 /SenseRes);
}

//==========================================================================
/* @brief  Gets the Battery Voltage value in volts */
float GetVoltageCell() 
{
  uint16_t valueDec=ReadRegister(MAX17055_REG_VCELL);
  return (valueDec * 0.000078125);
}
//==========================================================================
/*@brief  Gets the Battery SOC (State of Charge) value in Percent*/
float GetRepSOC() 
{	
	uint16_t valueDec =ReadRegister(MAX17055_REG_RepSOC);
  return (valueDec / 256);	
}
//===========================================================================
/*@brief  Gets the Battery Full Capacity value in mAh*/
float GetFullRepCap() 
{
	  uint16_t valueDec =ReadRegister(MAX17055_REG_FullRepCap);
  	return (valueDec*0.005/SenseRes);;	
}
//===========================================================================
/*! @brief  Gets the Battery Reported Capacity value in mAh */
float GetRepCap() 
{
	  uint16_t valueDec =ReadRegister(MAX17055_REG_RepCap);
  	return (valueDec*0.005/SenseRes);	
}

//===========================================================================
/*    @brief  Gets the Temperature value in Celcius */
float GetTemp() 
{	
	  int16_t valueDec = ReadRegister(MAX17055_REG_Temp); //Gets the raw Temperature value (16-bit signed integer, so +-32767)
  	return (valueDec / 256.0) ;	
}
//===========================================================================
/*@brief  Gets the Time to Empty value in hours*/
float GetTTE() 
{	
	  uint16_t valueDec =ReadRegister(MAX17055_REG_TTE);
  	return (valueDec / 640.002) ;	
}
//===========================================================================
/*@brief  Gets the Time to Full value in hours*/
float GetTTF() 
{	
	  int16_t valueDec = ReadRegister(MAX17055_REG_TTF);
  	return valueDec / 640.002 ;	
}
//===========================================================================
void MAX17055_Reset() 
{
	WriteRegister(MAX17055_REG_Status_Reset, MAX17055_Status_Reset);
}	
//===========================================================================
void MAX17055_Init(void) 
{
			 //Check POR
			 StatusPOR = ReadRegister(0x00) & 0x0002;
		   if(!(StatusPOR ==0))
			   {
				   while(ReadRegister(0x3D)&0x0001)  // IC clears this bit indicating the fuel gauge calculations are now up to date. This takes 710ms from power-up
							{					 
								HAL_Delay(10);               
							}
					 HibCFG=ReadRegister(0xBA);         // Store original HibCFG value
           WriteRegister (0x60 , 0x0090);       // Exit Hibernate Mode step 1(Wakes up the fuel gauge from hibernate mode, to reduce the response time of the IC to configuration changes)
           WriteRegister (0xBA , 0x0000);       // Exit Hibernate Mode step 2
           WriteRegister (0x60 , 0x0000);       // Exit Hibernate Mode step 3(Clears all commands)
							
					 WriteRegister (0x18 , DesignCap);      // Write DesignCap
					 WriteRegister (0x45 , DesignCap/32);   // Write dQAcc
				   WriteRegister (0x1E , IChgTerm);       // Write IchgTerm
					 WriteRegister (0x3A , VEmpty);         // Write VEmpty
							
           if(ChargeVoltage > 4.275)
							 {
									WriteRegister (0x46,dQAcc*51200/DesignCap);  // Write dPAcc
									WriteRegister (0xDB , 0x8400) ;              // Write ModelCFG ModelID=0 and VCH>4.25
							 }
					 else
					     {
							    WriteRegister (0x46 , dQAcc*44138/DesignCap); //Write dPAcc
							    WriteRegister (0xDB , 0x8000) ;               // Write ModelCFG ModelID=0 and VCH=4.2	
					     }						 
				  //wait "Refresh"to be cleared by firmware
					 while(ReadRegister(0xDB)&0x8000) 
					    {
								HAL_Delay(10);                                        //10ms wait loop. Do not continue until ModelCFG.Refresh == 0
							}
					 WriteRegister (0xBA , HibCFG) ;                           // Restore Original HibCFG value
							//Step 4 : Clear the POR bit to indicate that the custom model and parameters were successfully loaded
					 Status = ReadRegister(0x00);                              //Read Status
           WriteRegister (0x00, Status & 0xFFFD) ;                   //Write and Verify Status with POR bit cleared
							//Step 4.1: Identify the battery
							
					    //Step4.2 Check for MAX17055 reset
					StatusPOR = ReadRegister(0x00) & 0x0002;                   //Read POR bit in Status register
					
				 }
}
//===========================================================================
