/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
#define ON               1
#define OFF              0
#define TRUE             1
#define FALSE            0
#define uDISPLAY_ADD    (0x06 << 1) 
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
typedef enum {
              CMD_NONE,	
              CMD_ZOOM_LVL,
              CMD_ZOOM_TYPE,	
	            CMD_CONTRAST,
	            CMD_BRIGHTNESS,
	            CMD_AGC_MANUAL,
	            CMD_AGC_AUTO,	
	            CMD_POLARITY,
              CMD_CAL_TYPE,	
              CMD_RETICLE_TYPE,	
              CMD_RETICLE_MODE,	
              CMD_ELV_UP,	
              CMD_WND_RIGHT,	
	            CMD_ELV_DOWN,	
              CMD_WND_LEFT,	
              CMD_FACTORY_RESET,
	            CMD_DISPLAY_LUM,
	            CMD_SAVE_SETTINGS,
              }DISPLAY_CMDS; 

   typedef enum
		        {
              KEY1_SHORT_PRESS,	
              KEY2_SHORT_PRESS,		
              KEY3_SHORT_PRESS,
	            KEY4_SHORT_PRESS,
              KEY1_LONG_PRESS ,	
              KEY2_CONT_PRESS ,		
              KEY3_LONG_PRESS ,
	            KEY4_CONT_PRESS ,	
              KEY2_KEY4_PRESS ,							
	            NO_PRESS        ,
            }PRESS_FUNCTION; 								
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ONOFF_1_8V_Pin GPIO_PIN_13
#define ONOFF_1_8V_GPIO_Port GPIOC
#define MCU_TXD_Pin GPIO_PIN_2
#define MCU_TXD_GPIO_Port GPIOA
#define MCU_RXD_Pin GPIO_PIN_3
#define MCU_RXD_GPIO_Port GPIOA
#define KEY4_M_Pin GPIO_PIN_4
#define KEY4_M_GPIO_Port GPIOA
#define KEY4_M_EXTI_IRQn EXTI4_15_IRQn
#define KEY3_UP_Pin GPIO_PIN_5
#define KEY3_UP_GPIO_Port GPIOA
#define KEY3_UP_EXTI_IRQn EXTI4_15_IRQn
#define KEY2_DOWN_Pin GPIO_PIN_6
#define KEY2_DOWN_GPIO_Port GPIOA
#define KEY2_DOWN_EXTI_IRQn EXTI4_15_IRQn
#define KEY1_C_Pin GPIO_PIN_7
#define KEY1_C_GPIO_Port GPIOA
#define KEY1_C_EXTI_IRQn EXTI4_15_IRQn
#define BAT_SCL_Pin GPIO_PIN_10
#define BAT_SCL_GPIO_Port GPIOB
#define BAT_SDA_Pin GPIO_PIN_11
#define BAT_SDA_GPIO_Port GPIOB
#define ADV_RST_Pin GPIO_PIN_12
#define ADV_RST_GPIO_Port GPIOB
#define PWR_ON_Pin GPIO_PIN_13
#define PWR_ON_GPIO_Port GPIOB
#define DIS_SCL_Pin GPIO_PIN_9
#define DIS_SCL_GPIO_Port GPIOA
#define DIS_SDA_Pin GPIO_PIN_10
#define DIS_SDA_GPIO_Port GPIOA
#define INTRQ_Pin GPIO_PIN_3
#define INTRQ_GPIO_Port GPIOB
#define INTRQ_EXTI_IRQn EXTI2_3_IRQn
/* USER CODE BEGIN Private defines */
void my_printf(const char *fmt, ...);
void RETICLE_READJUST(int16_t wnd_shift,int16_t elv_shift);
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
