#ifndef _FPGA_H_
#define _FPGA_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "stm32l0xx_hal.h"
#include "usart.h"
#include <stdio.h>
#include <string.h>
				
#define  Other_Error                0xF2
#define  Operation_Word_Error       0xF7
#define  command_Support_Read       0xF8
#define  command_Support_Write      0xF9
#define  command_Support_Setting    0xFA
#define  Error_Command              0xFB
#define  SC_Error                   0xFD
#define  Process_End_Error          0xFE
#define  Process_Start_Error        0xFF
//==================================================================================================================

void     FPGA_COM_Init(void);
int      FPGA_TX_Command(uint8_t arr[],uint16_t Timeout);

#ifdef __cplusplus
}
#endif

#endif
