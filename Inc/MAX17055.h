#include "stm32l0xx_hal.h"

#define MAX17055_ADDRESS                            (0x36 << 1)    // Slave address 0x6C (or 0x36 for 7 MSbit address)
   
#define MAX17055_REG_Status_Reset                   (0x00)         // Set:0x0020, Reset:0x0000 (POR)

#define MAX17055_Status_Reset                     	(0x0002)      // Reset Bit
#define MAX17055_Status_Reset_POR                	  (0x0000)       // Reset Bit
	
#define MAX17055_REG_CONFIG_DesignCap               (0x18)         // Battery design capacity  (DesignCap)
#define DesignCap                                   (0x1F40)       // for 4000 mAH battery, 4000 /LSB = 4000 / (0.005/0.01))
	
#define MAX17055_REG_CONFIG_FullCapRep              (0x10)         //  Battery Full capacity capacity  (FullCapRep)
#define FullCapRep                                  (0x09C4)       //  
    
#define MAX17055_REG_CONFIG_dQAcc                   (0x45)         //  Battery design capacity /32  (DesignCap /32 ---> dQAcc
#define dQAcc                                       (0x00FA)       //  
    
#define MAX17055_REG_CONFIG_dPAcc                   (0x46)         //  0x0190 - dPAcc
#define dPAcc                                       (0x0190)       //  
	
#define MAX17055_REG_CONFIG_IChgTerm                (0x1E)         //  0x0080 - Battery charge termination
#define IChgTerm                                    (0x0500)       //  200mA
    
#define MAX17055_REG_CONFIG_VEmpty                  (0x3A)          // Battery VEmpty
#define VEmpty                                      (0x8C00)        // 2.8V
    
#define MAX17055_REG_CONFIG_Miscfg                  (0x2B)          // Battery Miscfg
#define MAX17055_CONFIG_Miscfg                      (0x3870)        // 0x3870 - Battery Miscfg
#define MAX17055_CONFIG_Miscfg_Quick_Start          (0x1400)        // 0x3870 - Battery Miscfg
    
#define MAX17055_REG_CONFIG_ModelCfg                (0xDB)          // 0x0420 - Battery ModelCfg
#define MAX17055_CONFIG_ModelCfg                    (0x0420)        // 0x0420 - Battery ModelCfg
    
#define MAX17055_REG_CONFIG_RComp0                  (0x38)          // 0x004E - Battery RComp0
#define MAX17055_CONFIG_RComp0                      (0x004E)        // 0x004E - Battery RComp0

#define MAX17055_REG_CONFIG_TempCo                  (0x39)    // 0x263E - Battery TempCo
#define MAX17055_CONFIG_TempCo                      (0x263E)  // 0x263E - Battery TempCo
	
#define MAX17055_REG_Command_HipCfg                 (0x60)    // 0x0090 - 0x0000 - exit Hibernate mode step 1 & 3 - COMMAND HipCfg
#define MAX17055_Command_HipCfg_Exit1               (0x0090)  // 0x0090 - 0x0000 - exit Hibernate mode step 1 & 3 - COMMAND HipCfg
#define MAX17055_Command_HipCfg_Exit3               (0x0000)  // 0x0090 - 0x0000 - exit Hibernate mode step 1 & 3 - COMMAND HipCfg
    
#define MAX17055_REG_CONFIG_HipCfg                  (0xBA)    // 0x0000 - exit Hibernate mode step 2 (Active mode)
#define MAX17055_CONFIG_HipCfg                      (0x0000)  // 0x0000 - exit Hibernate mode step 2 (Active mode)
 
#define ChargeVoltage                               (4.2)
#define MAX17055_REG_VCELL                          (0x09)
#define MAX17055_REG_CURRENT                        (0x0A)
#define MAX17055_REG_AVGCURRENT                     (0x0B)
#define MAX17055_REG_RepSOC                         (0x06)
#define MAX17055_REG_RepCap                         (0x05)
#define MAX17055_REG_FullRepCap                     (0x10)
#define MAX17055_REG_Temp                           (0x08)
#define MAX17055_REG_TTE                            (0x11)
#define MAX17055_REG_TTF                            (0x20)

#define SenseRes                                    (0.01)
#define MAX17055_FSTAT_REG                           0x3D
		

void    MAX17055_init(void);
void    MAX17055_Reset(void);
void    SetConfigRegister(void);
float   GetVoltageCell(void);
float   GetRepSOC(void);
float   GetCurrent_mA(void);
float   GetAvgCurrent_mA(void);
float   GetFullRepCap(void);
float   GetRepCap(void);
float   GetTemp(void);
float   GetTTE(void);
float   GetTTF(void);

_Bool    WriteRegister(uint8_t reg, uint16_t value);
uint16_t ReadRegister(uint8_t reg) ;
